import tensorflow as tf
from model import CycleGAN
from datetime import datetime
import os
import logging
from utils import ImagePool
import time

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_integer('batch_size', 1, 'batch size, default: 1')
tf.flags.DEFINE_bool('use_lsgan', True,
                     'use lsgan (mean squared error) or cross entropy loss, default: True')
tf.flags.DEFINE_string('norm', 'instance',
                       '[instance, batch] use instance norm or batch norm, default: instance')
tf.flags.DEFINE_integer('lambda1', 10.0,
                        'weight for forward cycle loss (X->Y->X), default: 10.0')
tf.flags.DEFINE_integer('lambda2', 10.0,
                        'weight for backward cycle loss (Y->X->Y), default: 10.0')
tf.flags.DEFINE_float('learning_rate', 2e-4,
                      'initial learning rate for Adam, default: 0.0002')
tf.flags.DEFINE_float('beta1', 0.5,
                      'momentum term of Adam, default: 0.5')
tf.flags.DEFINE_float('pool_size', 50,
                      'size of image buffer that stores previously generated images, default: 50')
tf.flags.DEFINE_integer('ngf', 64,
                        'number of gen filters in first conv layer, default: 64')

tf.flags.DEFINE_string('X', 'data/tfrecords/FullAgray.tfrecords',
                       'X tfrecords file for training, default: data/tfrecords/apple.tfrecords')
tf.flags.DEFINE_string('Y', 'data/tfrecords/FullBgray.tfrecords',
                       'Y tfrecords file for training, default: data/tfrecords/orange.tfrecords')
tf.flags.DEFINE_string('load_model', None,
                        'folder of saved model that you wish to continue training (e.g. 20170602-1936), default: None')

tf.flags.DEFINE_bool('use_float16', True,
                     'use float16 instead float32, default: True')
tf.flags.DEFINE_bool('use_multiGPU', False,
                     'use 2 gpu, default: False')
tf.flags.DEFINE_bool('use_Grayscale', True,
                     'use Grayscale images instead rgb, default: True')
tf.flags.DEFINE_integer('HEIGHT', 128,
                        'HEIGHT')
tf.flags.DEFINE_integer('WIDTH', 256,
                        'WIDTH')

def train():
  if FLAGS.load_model is not None:
    checkpoints_dir = "checkpoints/" + FLAGS.load_model.lstrip("checkpoints/")
  else:
    current_time = datetime.now().strftime("%Y%m%d-%H%M")
    checkpoints_dir = "checkpoints/{}".format(current_time)
    try:
      os.makedirs(checkpoints_dir)
    except os.error:
      pass


  graph = tf.Graph()
  with graph.as_default():
    cycle_gan = CycleGAN(
        X_train_file=FLAGS.X,
        Y_train_file=FLAGS.Y,
        batch_size=FLAGS.batch_size,
        use_lsgan=FLAGS.use_lsgan,
        norm=FLAGS.norm,
        lambda1=FLAGS.lambda1,
        lambda2=FLAGS.lambda2,
        learning_rate=FLAGS.learning_rate,
        beta1=FLAGS.beta1,
        ngf=FLAGS.ngf,
        use_multiGPU=FLAGS.use_multiGPU,
        use_float16 = FLAGS.use_float16,
        height = FLAGS.HEIGHT,
        width = FLAGS.WIDTH,
        gray = FLAGS.use_Grayscale
    )
    G_loss, D_Y_loss, F_loss, D_X_loss, fake_y, fake_x = cycle_gan.model()
    optimizers = cycle_gan.optimize(G_loss, D_Y_loss, F_loss, D_X_loss)

    summary_op = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter(checkpoints_dir, graph)
    saver = tf.train.Saver()

  print("Start training.")

  config = tf.ConfigProto()
  config.gpu_options.allow_growth = True
  #config.log_device_placement=True

  with tf.Session(graph=graph, config = config) as sess:
    if FLAGS.load_model is not None:
      checkpoint = tf.train.get_checkpoint_state(checkpoints_dir)
      meta_graph_path = checkpoint.model_checkpoint_path + ".meta"
      restore = tf.train.import_meta_graph(meta_graph_path)
      restore.restore(sess, tf.train.latest_checkpoint(checkpoints_dir))
      step = int(meta_graph_path.split("-")[2].split(".")[0])
    else:
      sess.run(tf.global_variables_initializer())
      step = 0

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    
    t = time.time()
    try:
      while not coord.should_stop():
        # train
        _, G_loss_val, D_Y_loss_val, F_loss_val, D_X_loss_val = (
              sess.run(
                  [optimizers, G_loss, D_Y_loss, F_loss, D_X_loss]
              )
        )

        if step % 100 == 0:
          _, G_loss_val, D_Y_loss_val, F_loss_val, D_X_loss_val, summary = sess.run([optimizers, G_loss, D_Y_loss, F_loss, D_X_loss, summary_op])

          logging.info('-----------Step %d:-------------' % step)
          logging.info('  G_loss   : {}'.format(G_loss_val))
          logging.info('  D_Y_loss : {}'.format(D_Y_loss_val))
          logging.info('  F_loss   : {}'.format(F_loss_val))
          logging.info('  D_X_loss : {}'.format(D_X_loss_val))
          logging.info('  Time : {} secs'.format(time.time() - t))

          t = time.time()
          train_writer.add_summary(summary, step)
          train_writer.flush()

        if step % 5000 == 0:
          save_path = saver.save(sess, checkpoints_dir + "/model.ckpt", global_step=step)
          logging.info("Model saved in file: %s" % save_path)

        if step == 21000:
            break

        step += 1

    except KeyboardInterrupt:
      logging.info('Interrupted')
      coord.request_stop()
    except Exception as e:
      coord.request_stop(e)
    finally:
      save_path = saver.save(sess, checkpoints_dir + "/model.ckpt", global_step=step)
      logging.info("Model saved in file: %s" % save_path)
      # When done, ask the threads to stop.
      coord.request_stop()
      coord.join(threads)

def main(unused_argv):
  logging.info("Start program at {}".format(datetime.now()))
  logging.info("with options:")
  logging.info('batch_size: {}'.format(FLAGS.batch_size))
  logging.info('use_lsgan: {}'.format(FLAGS.use_lsgan))
  logging.info('norm: {}'.format(FLAGS.norm))
  logging.info('lambda1: {}'.format(FLAGS.lambda1))
  logging.info('lambda2: {}'.format(FLAGS.lambda2))
  logging.info('learning_rate: {}'.format(FLAGS.learning_rate))
  logging.info('ngf: {}'.format(FLAGS.ngf))
  logging.info('X data: {}'.format(FLAGS.Y))
  logging.info('Y data: {}'.format(FLAGS.X))
  logging.info('use_float16: {}'.format(FLAGS.use_float16))
  logging.info('use_multiGPU: {}'.format(FLAGS.use_multiGPU))
  logging.info('use_Grayscale: {}'.format(FLAGS.use_Grayscale))
  logging.info('HEIGHT: {}'.format(FLAGS.HEIGHT))
  logging.info('WIDTH: {}'.format(FLAGS.WIDTH))
  train()

if __name__ == '__main__':
  logging.basicConfig(level=logging.INFO)
  tf.app.run()
