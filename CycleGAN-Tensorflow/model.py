import tensorflow as tf
import ops
import utils
from reader import Reader
from discriminator import Discriminator
from generator import Generator

REAL_LABEL = 0.9

class CycleGAN:
  def __init__(self,
               X_train_file='',
               Y_train_file='',
               batch_size=1,
               use_lsgan=True,
               norm='instance',
               lambda1=10.0,
               lambda2=10.0,
               learning_rate=2e-4,
               beta1=0.5,
               ngf=64,
               use_multiGPU=False,
               use_float16=False,
               height = 128,
               width = 128,
               gray = False
              ):
    """
    Args:
      X_train_file: string, X tfrecords file for training
      Y_train_file: string Y tfrecords file for training
      batch_size: integer, batch size
      lambda1: integer, weight for forward cycle loss (X->Y->X)
      lambda2: integer, weight for backward cycle loss (Y->X->Y)
      use_lsgan: boolean
      norm: 'instance' or 'batch'
      learning_rate: float, initial learning rate for Adam
      beta1: float, momentum term of Adam
      ngf: number of gen filters in first conv layer
      use_multiGPU: use 2 gpu
      use_float16: use float 16 instead float 32
      gray: use grayscale images instead rgb
    """
    self.lambda1 = lambda1
    self.lambda2 = lambda2
    self.use_lsgan = use_lsgan
    use_sigmoid = not use_lsgan
    self.batch_size = batch_size
    self.learning_rate = learning_rate
    self.beta1 = beta1
    self.X_train_file = X_train_file
    self.Y_train_file = Y_train_file
    self.use_multiGPU = use_multiGPU
    self.use_float16 = use_float16
    self.height = height
    self.width = width
    self.gray = gray

    self.is_training = tf.placeholder_with_default(True, shape=[], name='is_training')

    with tf.device('/gpu:0'):
        self.G = Generator('G', self.is_training, ngf=ngf, norm=norm, use_float16 = self.use_float16, height = self.height, width = self.width, gray = self.gray)
        self.D_Y = Discriminator('D_Y', self.is_training, norm=norm, use_sigmoid=use_sigmoid, use_float16 = self.use_float16)
    with tf.device('/gpu:1' if self.use_multiGPU else '/gpu:0'):
        self.F = Generator('F', self.is_training, ngf=ngf, norm=norm, use_float16 = self.use_float16, height = self.height, width = self.width, gray = self.gray)
        self.D_X = Discriminator('D_X', self.is_training, norm=norm, use_sigmoid=use_sigmoid, use_float16 = self.use_float16)

  def model(self):
    with tf.device('/cpu:0'):
        X_reader = Reader(self.X_train_file, name='X',
            batch_size=self.batch_size, num_threads = 2, use_float16 = self.use_float16, height = self.height, width = self.width, gray = self.gray)
        Y_reader = Reader(self.Y_train_file, name='Y',
            batch_size=self.batch_size, num_threads = 2, use_float16 = self.use_float16, height = self.height, width = self.width, gray = self.gray)

        x = X_reader.feed()
        y = Y_reader.feed()

    with tf.device('/gpu:0'):
        Fy = self.F(y)
        fake_x = Fy
        GFy = self.G(Fy)
    with tf.device('/gpu:1' if self.use_multiGPU else '/gpu:0'):
        Gx = self.G(x)
        fake_y = Gx
        FGx = self.F(Gx)

    with tf.device('/cpu:0'):
        Dy = self.D_Y(y)
        Dy_fake = self.D_Y(Gx)
        
        Dx = self.D_X(x)
        Dx_fake = self.D_X(Fy)

        cycle_loss = self.cycle_consistency_loss(FGx, GFy, x, y)

        # X -> Y
        G_gan_loss = self.generator_loss(Dy_fake, use_lsgan=self.use_lsgan)
        G_loss =  G_gan_loss + cycle_loss
        D_Y_loss = self.discriminator_loss(Dy, Dy_fake, use_lsgan=self.use_lsgan)

        # Y -> X
        F_gan_loss = self.generator_loss(Dx_fake, use_lsgan=self.use_lsgan)
        F_loss = F_gan_loss + cycle_loss
        D_X_loss = self.discriminator_loss(Dx, Dx_fake, use_lsgan=self.use_lsgan)  

        # summary
        tf.summary.image('X/generated', utils.batch_convert2int(Gx))
        tf.summary.image('X/reconstruction', utils.batch_convert2int(FGx))
        tf.summary.image('Y/generated', utils.batch_convert2int(Fy))
        tf.summary.image('Y/reconstruction', utils.batch_convert2int(GFy))

        tf.summary.histogram('D_Y/true', Dy)
        tf.summary.histogram('D_Y/fake', Dy_fake)
        tf.summary.histogram('D_X/true', Dx)
        tf.summary.histogram('D_X/fake', Dx_fake)

        tf.summary.scalar('loss/G', G_gan_loss)
        tf.summary.scalar('loss/D_Y', D_Y_loss)
        tf.summary.scalar('loss/F', F_gan_loss)
        tf.summary.scalar('loss/D_X', D_X_loss)
        tf.summary.scalar('loss/cycle', cycle_loss)

    return G_loss, D_Y_loss, F_loss, D_X_loss, fake_y, fake_x

  def optimize(self, G_loss, D_Y_loss, F_loss, D_X_loss):
    def make_optimizer(loss, variables, name='Adam'):
      with tf.device('/cpu:0'):
          """ Adam optimizer with learning rate 0.0002 for the first 100k steps (~100 epochs)
              and a linearly decaying rate that goes to zero over the next 100k steps
          """
          global_step = tf.Variable(0, trainable=False, name="GlobStep")
          starter_learning_rate = self.learning_rate
          end_learning_rate = 0.0#1e-08 if self.use_float16 else 0.0
          start_decay_step = 20000
          decay_steps = 1000 if self.use_float16 else 100000
          beta1 = self.beta1
          learning_rate = (
                tf.where(
                        tf.greater_equal(global_step, start_decay_step),
                        tf.train.polynomial_decay(starter_learning_rate, global_step-start_decay_step,
                                                decay_steps, end_learning_rate,
                                                power=1.0),
                        starter_learning_rate
                )

            )
          tf.summary.scalar('learning_rate/{}'.format(name), learning_rate)

      learning_step = (
          tf.train.AdamOptimizer(learning_rate, beta1=beta1, name=name, epsilon = 1e-04 if self.use_float16 else 1e-08)
                  .minimize(loss, global_step=global_step, var_list=variables)
      )
      return learning_step

    with tf.device('/gpu:0'):
        G_optimizer = make_optimizer(G_loss, self.G.variables, name='Adam_G')
        D_Y_optimizer = make_optimizer(D_Y_loss, self.D_Y.variables, name='Adam_D_Y')
    with tf.device('/gpu:1' if self.use_multiGPU else '/gpu:0'):
        F_optimizer =  make_optimizer(F_loss, self.F.variables, name='Adam_F')
        D_X_optimizer = make_optimizer(D_X_loss, self.D_X.variables, name='Adam_D_X')

    with tf.control_dependencies([G_optimizer, D_Y_optimizer, F_optimizer, D_X_optimizer]):
      return tf.no_op(name='optimizers')

  def discriminator_loss(self, D, D_fake, use_lsgan=True):
    """ Note: default: D(y).shape == (batch_size,5,5,1),
                       fake_buffer_size=50, batch_size=1
    Args:
      G: generator object
      D: discriminator object
    Returns:
      loss: scalar
    """
    if use_lsgan:
      # use mean squared error
      error_real = tf.reduce_mean(tf.squared_difference(D, REAL_LABEL))
      error_fake = tf.reduce_mean(tf.square(D_fake))
    else:
      # use cross entropy
      error_real = -tf.reduce_mean(ops.safe_log(D, 1e-04 if self.use_float16 else 1e-12))
      error_fake = -tf.reduce_mean(ops.safe_log(1-D_fake, 1e-04 if self.use_float16 else 1e-12))
    loss = (error_real + error_fake) / 2
    return loss

  def generator_loss(self, D, use_lsgan=True):
    """  fool discriminator into believing that G(x) is real
    """
    if use_lsgan:
      # use mean squared error
      loss = tf.reduce_mean(tf.squared_difference(D, REAL_LABEL))
    else:
      # heuristic, non-saturating loss
      loss = -tf.reduce_mean(ops.safe_log(D, 1e-04 if self.use_float16 else 1e-12)) / 2
    return loss

  def cycle_consistency_loss(self, FGx, GFy, x, y):
    """ cycle consistency loss (L1 norm)
    """
    forward_loss = tf.reduce_mean(tf.abs(FGx-x))
    backward_loss = tf.reduce_mean(tf.abs(GFy-y))
    loss = self.lambda1*forward_loss + self.lambda2*backward_loss
    return loss
