"""Translate an image to another image
An example of command-line usage is:
python export_graph.py --model pretrained/apple2orange.pb \
                       --input input_sample.jpg \
                       --output output_sample.jpg \
"""

import tensorflow as tf
import os
from model import CycleGAN
import utils

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('model', './pretrained/A2B.pb', 'model path (.pb)')
tf.flags.DEFINE_string('input', 'input_sample.jpg', 'input image path (.jpg)')
tf.flags.DEFINE_string('output', 'output_sample.jpg', 'output image path (.jpg)')

tf.flags.DEFINE_bool('use_float16', False,
                     'use float16 instead float32, default: True')
tf.flags.DEFINE_bool('use_Grayscale', False,
                     'use Grayscale images instead rgb, default: True')
tf.flags.DEFINE_integer('HEIGHT', 576,
                        'HEIGHT')
tf.flags.DEFINE_integer('WIDTH', 832,
                        'WIDTH')

def list_files(in_path):
    files = []
    for (dirpath, dirnames, filenames) in os.walk(in_path):
        files.extend(filenames)
        break
    return files

def prepare_image(path, use_float16 = False, height = 128, width = 128, gray = False):
    with tf.gfile.FastGFile(path, 'rb') as f:
        image_data = f.read()
        input_image = tf.image.decode_jpeg(image_data, channels=1 if gray else 3)
        input_image = tf.image.resize_images(input_image, size=(height, width))
        input_image = utils.convert2float(input_image, use_float16 = use_float16)
        input_image.set_shape([height, width, 1 if gray else 3])
    return input_image

def inference(use_float16 = False, height = 128, width = 128, gray = False):
  InputFolder = 'D:/Temp/SimpleHardSofasData/Hard/'
  OutputFolder = 'D:/Temp/SimpleHardSofasData/SimpleGenerated/'
  input_images = list_files(InputFolder)

  if not os.path.exists(OutputFolder):
        os.makedirs(OutputFolder)

  graph = tf.Graph()

  with graph.as_default():
    with tf.gfile.FastGFile(FLAGS.model, 'rb') as model_file:
      input = tf.placeholder(dtype=tf.float16 if use_float16 else tf.float32, shape = [height, width, 1 if gray else 3])
      graph_def = tf.GraphDef()
      graph_def.ParseFromString(model_file.read())
    [output_image] = tf.import_graph_def(graph_def,
                          input_map={'input_image': input},
                          return_elements=['output_image:0'],
                          name='output')

  with tf.Session(graph=graph) as sess:
      for image_path in input_images:
        out_path = os.path.join(OutputFolder,image_path)
        image_path = os.path.join(InputFolder,image_path)
        print(image_path)
        input_tensor = prepare_image(image_path, use_float16 = use_float16, height = height, width =width, gray = gray)
        input_image = input_tensor.eval()
        generated = output_image.eval(feed_dict = {input: input_image})
        with open(out_path, 'wb') as f:
          f.write(generated)

def main(unused_argv):
  inference(use_float16 = FLAGS.use_float16, height = FLAGS.HEIGHT, width = FLAGS.WIDTH, gray = FLAGS.use_Grayscale)

if __name__ == '__main__':
  tf.app.run()