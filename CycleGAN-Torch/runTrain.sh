DATA_ROOT=/root/Data/GAN/ContrastGray/ name=ContrastGray2 batchSize=4 gpu=1 nThreads=4 lambda_A=1 lambda_B=1 use_optnet=1 th train.lua
python3 /root/Source/mail.py "Contrast gray 2 finished (with lambda 1)"
DATA_ROOT=/root/Data/GAN/Eleg/ name=Eleg batchSize=4 gpu=1 nThreads=4 use_optnet=1 th train.lua
python3 /root/Source/mail.py "Eleg finished"
DATA_ROOT=/root/Data/GAN/Ven/ name=Ven batchSize=4 gpu=1 nThreads=4 use_optnet=1 th train.lua
python3 /root/Source/mail.py "Ven finished"