import tensorflow as tf
import cv2
import numpy as np
import matplotlib
import math
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import autoencoderRSepFinal as autoencoder


from imgaug import augmenters as iaa
from imgaug import imgaug

class Segmentator:
    def __init__(self,
                 layers,
                 inputShape=[256, 320, 3],
                 LoadEncoderPath=None, 
                 LoadClassPath=None,
                 LoadDecoderPath=None,
                 SharedW=True,
                 ClassNumber=9):
        if (LoadEncoderPath == None or LoadDecoderPath == None):
            print("Wrong checkpoint")
            return -1

        self.__graph = tf.Graph()
        with self.__graph.as_default():
            self.__sess = tf.Session()
            network = autoencoder.Network(layers, inputShape, False, ClassNumber)
            network.Reverse(layers, False, SharedW)

            EncoderLoader = tf.train.Saver(network.ENCODER_vars)
            DecoderLoader = tf.train.Saver(network.DECODER_vars)
            if LoadClassPath:
                ClassLoader = tf.train.Saver(network.CLASS_vars)

            print('Restoring encoder: {}'.format(LoadEncoderPath))
            EncoderLoader.restore(self.__sess, LoadEncoderPath)

            print('Restoring decoder: {}'.format(LoadDecoderPath))
            DecoderLoader.restore(self.__sess, LoadDecoderPath)

            if LoadClassPath:
                print('Restoring classifier: {}'.format(LoadClassPath))
                ClassLoader.restore(self.__sess, LoadClassPath)
            
            self.__fetch = []
            self.__fetch.append(network.segmentation_result)
            if LoadClassPath:
                self.__fetch.append(network.classResult)

            self.__inputImg = network.inputs

    def SegmentImg(self, imgBGR):
        imgBGR = np.reshape(imgBGR, (1,) + imgBGR.shape)
        with self.__graph.as_default():
            return self.__sess.run(self.__fetch, feed_dict={self.__inputImg : imgBGR})

    def __del__(self):
        self.__sess.close()

class ImageProcessor:
    MaxSize = 5000
    def __init__(self, inputShape=[256, 320, 3], PreBlurRate = 5, PostBlurRate = 30):
        self.HEIGHT = inputShape[0]
        self.WIDTH = inputShape[1]
        self.DEPTH = inputShape[2]

        TransformationSeq = iaa.Sequential([
            iaa.GaussianBlur(PreBlurRate),
            iaa.AdditiveGaussianNoise(scale=self.PreNoiseRate * 255, per_channel = True),
        ])
        self.Transform = TransformationSeq.to_deterministic()
        
        PostProcessingSeq = iaa.Sequential([
            iaa.GaussianBlur(PostBlurRate, name="GaussianBlur"),
        ])
        self.PostProcess = PostProcessingSeq.to_deterministic()

    def LoadBGR(self, ImgPath, changeShape=False, FitInBounds=False):
        image = cv2.imread(ImgPath, 1)
        if changeShape:
            self.DEPTH = image.shape[2]
            if FitInBounds:
                if self.WIDTH > ImageProcessor.MaxSize:
                    k = self.WIDTH / ImageProcessor.MaxSize
                    self.HEIGHT = int(image.shape[0] / k)
                    self.WIDTH = int(ImageProcessor.MaxSize)
                else:
                    self.HEIGHT = image.shape[0]
                    self.WIDTH = image.shape[1]

        image = cv2.resize(image, (self.WIDTH, self.HEIGHT))
        return image

    def LoadMask(self, MaskPath):
        target_image = cv2.imread(MaskPath, 0)
        target_image = cv2.resize(target_image, (self.WIDTH, self.HEIGHT))
        target_image = np.array(target_image)
        target_image = np.reshape(target_image, [self.HEIGHT, self.WIDTH])
        target_image = self.ThresholdMask(target_image)
        return target_image

    def BGRtoRGB(self, imgBGR):
        imgRGB = cv2.cvtColor(imgBGR, cv2.COLOR_BGR2RGB)
        return imgRGB

    def SaveImg(self, savePath, Img):
        fig, axs = plt.subplots(nrows=1, ncols=1)
        axs.get_xaxis().set_visible(False)
        axs.get_yaxis().set_visible(False)
        if self.__IsColored(Img):
            axs.imshow(Img)
        else:
            axs.imshow(Img[:,:,0], cmap='gray', vmin=0, vmax=1)
        fig.savefig(savePath,bbox_inches='tight', dpi=1200)   # save the figure to file
        plt.close(fig)

    def __IsColored(self, Img):
        PixNumber = 1
        for i in range(len(Img.shape)):
            PixNumber *= Img.shape[i]
        if PixNumber == self.HEIGHT * self.WIDTH * self.DEPTH:
            return True
        elif PixNumber == self.HEIGHT * self.WIDTH:
            return False
        else:
            raise TypeError("Invalid dimensions for image data")

    def SaveCombo(self, savePath, Img1, Img2, Message = ""):
        fig, axs = plt.subplots(nrows=1, ncols=2)
        fig.suptitle(Message, fontsize=8)
        axs[0].get_xaxis().set_visible(False)
        axs[0].get_yaxis().set_visible(False)
        axs[1].get_xaxis().set_visible(False)
        axs[1].get_yaxis().set_visible(False)

        if self.__IsColored(Img1):
            axs[0].imshow(Img1, )
        else:
            axs[0].imshow(Img1[:,:,0], cmap='gray', vmin=0, vmax=1)

        if self.__IsColored(Img2):
            axs[1].imshow(Img2)
        else:
            axs[1].imshow(Img2[:,:,0], cmap='gray', vmin=0, vmax=1)

        fig.savefig(savePath,bbox_inches='tight', dpi=1200)   # save the figure to file
        plt.close(fig) 

    def EvalQuality(self, SegmImg, TargetImg):
        result = SegmImg == TargetImg
        result = np.sum(result)
        result /= (self.HEIGHT * self.WIDTH)
        return result

    def ThresholdMask(self, SegmImg):
        thresh = SegmImg > 0.5
        return thresh

    def Combine(self, Img, Style, Segment):
        if self.__IsColored(Img):
            Segment = np.stack([Segment, Segment,Segment], axis = -1)
            Colored = True
        else:
            Segment = [Segment]
            Colored = False
        Style = Style * Segment

        Segment = np.invert(Segment)
        Img = Img * Segment


        Result = Style + Img
        
        if not Colored:
            Result = np.reshape(Result, Result.shape[1:3])

        return Result

    def Resize(self, Img):
        Img = cv2.resize(Img, (self.WIDTH, self.HEIGHT),interpolation = cv2.INTER_NEAREST)
        return Img
    
    PreContrastRate = 0.5
    PreNoiseRate = 0.2
    # This function suppress borders before stylzation
    def PreProcessing(self, Img, mask = None):
        
        Img = self.set_contrast(self.PreContrastRate, Img, mask)

        #kernel = np.ones((5,5),np.uint8)
        #Img = cv2.erode(Img,kernel,iterations = 3)
        #Img = cv2.morphologyEx(Img, cv2.MORPH_OPEN, kernel) #OPEN устраняет светлые области, но на изображении появляются квадратные пятна (квадратные из-за формы матрицы). Устраняются сглаживанием

        Img = np.reshape(Img, (1,) + Img.shape)
        Img = self.Transform.augment_images(Img)
        Img = Img[0,:,:,:]

        return Img
    
    # This function transfer shadows and control color after stylization
    PostContrastRate = 1
    PostContentColorImage = "C:/Users/dimit/Downloads/IMG_8948_Extracted.JPG"
    PostUseMeanInsteadMax = False
    PostUseColorTransfer = True
    PostUseWBControl = True
    PostBrightnessAddition = 0
    PostUseShadowTransfer = True
    PostUseSmartContrastChooser = False
    def __SmartContrastChooser(self, Amplitude): return 135.0 / (45.0 + Amplitude)
    def PostProcessing(self, Img, StyleImg, mask = None):
        
        if self.PostUseShadowTransfer:        
            # Less blur for harder borders
            Shadow = np.reshape(Img, (1,) + Img.shape)
            Shadow = self.PostProcess.augment_images(Shadow)
            Shadow = Shadow[0,:,:,:]
        
            # Convert Img to shadows
            Shadow = cv2.cvtColor(Shadow, cv2.COLOR_RGB2GRAY)
            mask = np.array(mask, dtype = np.uint8)

            # If sofa has too big amplitude (too contrast), we should decrease its contrast
            if self.PostUseSmartContrastChooser:
                Min, Max, _, _ = cv2.minMaxLoc(Shadow, mask)
                Amplitude = Max-Min
                self.PostContrastRate = self.__SmartContrastChooser(Amplitude)

            # Contrast to make shadows harder (or softer for too contrast sofas)
            Shadow = self.set_contrast(self.PostContrastRate, Shadow, mask)
            
            # Calculation of sofa statistics
            Min, Max, _, _ = cv2.minMaxLoc(Shadow, mask)
            Amplitude = Max-Min
            Mean, Dev = cv2.meanStdDev(Shadow, mask = mask)
            Mean = Mean[0]
            Dev = Dev[0]
            print("Deviation: ", Dev, "Amplitude :", Amplitude)

            Shadow = np.array(Shadow, dtype = np.float)
            Shadow /= Mean if self.PostUseMeanInsteadMax else Max

            # If shadows too hard, this koef can help
            #Shadow -= 1.0
            #Shadow *= 0.8
            #Shadow += 1.0
        
            Shadow = cv2.merge([Shadow, Shadow, Shadow])

            StyleImg = np.array(StyleImg, dtype = np.float)
            StyleImg *= Shadow

            # If result is too dark, this can add brightness
            StyleImg += self.PostBrightnessAddition

            StyleImg = np.clip(StyleImg, 0, 255)
            StyleImg = np.array(StyleImg, dtype = np.uint8)

        if self.PostUseColorTransfer:
            # If result has wrong color, this can repair middle color (but if result is too contrast, this funtion looks bad)
            rawStyle = cv2.imread(self.PostContentColorImage)
            rawStyle = cv2.cvtColor(rawStyle, cv2.COLOR_BGR2RGB)
            StyleImg = self.transfer_color(rawStyle, StyleImg, mask)
        
        if self.PostUseWBControl:
            Img = cv2.cvtColor(Img, cv2.COLOR_BGR2RGB)
            StyleImg = self.transferWB(Img, StyleImg, mask)

        return StyleImg

    def transfer_color(self, source, target, mask = None):
        meanS = list(cv2.mean(source))[0:3]
        result = self.apply_color(meanS, target, mask)
        return result

    def apply_color(self, color, target, mask = None):
        if not mask is None:
            mask = np.array(mask, dtype = np.uint8)
        meanT = list(cv2.mean(target, mask))[0:3]
        k = [color[i] / meanT[i] for i in range(len(color))]

        result = np.array(target, dtype = np.float)
        result *= k
        result = np.clip(result, 0, 255)
        result = np.array(result, dtype = np.uint8)
        return result

    def set_contrast(self, contrast, img, mask = None):
        if not mask is None:
            mask = np.array(mask, dtype = np.uint8)
        ImgDepth = 3 if len(img.shape) == 3 else 1
        mean = list(cv2.mean(img, mask))[0:ImgDepth]

        result = np.array(img, dtype = np.float)

        result -= mean
        result *= contrast
        result += mean

        result = np.clip(result, 0, 255)
        result = np.array(result, dtype = np.uint8)

        return result

    WBNoiseRate = 0.02
    def transferWB(self, source, target, mask = None):
        SourceChannels = cv2.split(source)

        TargetChannels = np.array(target, dtype = np.float)
        TargetChannels /= 255.0
        TargetChannels = cv2.split(TargetChannels)

        if mask is None:
            SofaMask = None
        else:
            SofaMask = np.array(mask, dtype = np.uint8)

        for i in range(len(SourceChannels)):
            height, width = SourceChannels[i].shape
            vec_size = width * height
            flatImg = SourceChannels[i].reshape(vec_size)
            flatMask = mask.reshape(vec_size)

            temp = []
            for j in range(vec_size): # Maybe numpy has more fast meyhod for this
                if not flatMask[j]: temp.append(flatImg[j])
            vec_size = len(temp)
            flatImg = np.sort(temp)

            low_val  = flatImg[int(math.floor(vec_size * self.WBNoiseRate))]
            high_val = flatImg[math.ceil(vec_size * (1.0 - self.WBNoiseRate))]

            TargetChannels[i] *= (high_val - low_val)
            TargetChannels[i] += low_val

        result = cv2.merge(TargetChannels)
        result = np.clip(result, 0, 255)
        result = np.array(result, dtype = np.uint8)
        return result

    def getShadow(self, Img, mask = None):
        # Less blur for harder borders
        Shadow = np.reshape(Img, (1,) + Img.shape)
        Shadow = self.PostProcess.augment_images(Shadow)
        Shadow = Shadow[0,:,:,:]
        
        # Convert Img to shadows
        Shadow = cv2.cvtColor(Shadow, cv2.COLOR_RGB2GRAY)
        BYTEmask = np.array(mask, dtype = np.uint8)

        # If sofa has too big amplitude (too contrast), we should decrease its contrast
        if self.PostUseSmartContrastChooser:
            Min, Max, _, _ = cv2.minMaxLoc(Shadow, BYTEmask)
            Amplitude = Max-Min
            self.PostContrastRate = self.__SmartContrastChooser(Amplitude)

        # Contrast to make shadows harder (or softer for too contrast sofas)
        Shadow = self.set_contrast(self.PostContrastRate, Shadow, BYTEmask)


        empty = np.zeros(Shadow.shape, dtype = np.uint8)
        

        result = self.Combine(empty, Shadow, mask)

        return result

    def getOnlySofa(self, Img, mask = None):
        BYTEmask = np.array(mask, dtype = np.uint8)

        empty = np.zeros(Img.shape, dtype = np.uint8)
        
        result = self.Combine(empty, Img, mask)

        return result

import FastStyle
class FastStylizer:
    def __init__(self, StylePath, inputShape):
        self.__graph = tf.Graph()
        with self.__graph.as_default():
            self.__sess = tf.Session()
            self.__img_placeholder = tf.placeholder(tf.float32, shape=(1,) + tuple(inputShape))

            self.__preds = FastStyle.net(self.__img_placeholder)
            saver = tf.train.Saver()
            saver.restore(self.__sess, StylePath)

    def StyleImg(self, imgRGB):
        imgRGB = np.reshape(imgRGB, (1,) + imgRGB.shape)
        with self.__graph.as_default():
            _preds = self.__sess.run(self.__preds, feed_dict={self.__img_placeholder:imgRGB})
            imgStylized = np.clip(_preds[0], 0, 255).astype(np.uint8)
            return imgStylized

    def __del__(self):
        self.__sess.close()

     