import math
import os
import time
from math import ceil

import cv2
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from imgaug import augmenters as iaa
from imgaug import imgaug
from conv2d import Conv2d
from libs.activations import lrelu
from libs.activations import linear
import datetime
import io

class Network:
    def __init__(self, layers, inputShape = [256, 320, 3], trainable = False):
        self.IMAGE_HEIGHT = inputShape[0]
        self.IMAGE_WIDTH = inputShape[1]
        self.IMAGE_CHANNELS = inputShape[2]

        self.inputs = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.IMAGE_CHANNELS], name='inputs')
        self.targets = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, 1], name='targets')
        # добавить placeholder для классификации (в случае многослойного выхода)

        self.layers = {}

        # благодаря этой строчке не нужно вычитать средный пиксель из каждого фото
        list_of_images_norm = tf.map_fn(tf.image.per_image_standardization, self.inputs)
        net = tf.stack(list_of_images_norm)

        # ENCODER
        for layer in layers:
            self.layers[layer.name] = net = layer.create_layer(net)

        print("Current input shape: ", net.get_shape())

        layers.reverse()

        # ПОДМЕНА СЛОЯ!!!!!
        layers.pop()
        LastLayer = Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_0_0')
        layers.append(LastLayer)
        temp = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, 1], name='inputs2') #убрать этот placeholder
        self.layers[LastLayer.name] = LastLayer.create_layer(temp)
        # ПОДМЕНА СЛОЯ!!!!!
        
        Conv2d.reverse_global_variables()

        # DECODER
        for layer in layers:
            net = layer.create_layer_reversed(net, prev_layer=self.layers[layer.name], actFunc = lrelu if layer.name != 'conv_0_0' else linear)
        self.segmentation_result = tf.sigmoid(net) #заменить на softmax (если выход многослойный)

        print('segmentation_result.shape: {}, targets.shape: {}'.format(self.segmentation_result.get_shape(), self.targets.get_shape()))

        if trainable:
            # MSE loss
            self.cost = tf.sqrt(tf.reduce_mean(tf.square(self.segmentation_result - self.targets)))
            self.train_op = tf.train.AdamOptimizer().minimize(self.cost)
            # добавить еще один оптимизатор (для многослойного выхода)

def PrepareImage(test_image, network):
    # Добавить поворот вертикальных изображений
    return cv2.resize(test_image, (network.IMAGE_WIDTH, network.IMAGE_HEIGHT))

class Dataset:
    def __init__(self, batch_size, network, folder):
        self.batch_size = batch_size

        train_files, validation_files, test_files = self.train_valid_test_split(os.listdir(os.path.join(folder, 'inputs')))

        # Для многомлойности добавить тут массив classificationTargets
        print("Train images loading:")
        self.train_inputs, self.train_targets = self.file_paths_to_images(folder, train_files, network)
        print("Test images loading:")
        self.test_inputs, self.test_targets = self.file_paths_to_images(folder, test_files, network)

        assert len(self.train_inputs) == len(self.train_targets), "training data currapted"
        print("{0} training images".format(len(self.train_inputs)))
        assert len(self.test_inputs) == len(self.test_targets), "training data currapted"
        print("{0} test images".format(len(self.test_inputs)))

        self.pointer = 0

    # Если загружаем картинки из из заранее подгатовленного файла, то нужно заменить эту функцию.
    def file_paths_to_images(self, folder, files_list, network):
        inputs = []
        targets = []

        for i in range(len(files_list)):
            print("{}/{} images loaded".format(i,len(files_list)))

            file = files_list[i]
            input_image = os.path.join(folder, 'inputs', file)
            test_image = np.array(cv2.imread(input_image, 1))
            assert test_image.shape[0] <= test_image.shape[1], "Only horizontal images supported. Check this file: {}".format(input_image)
            test_image = PrepareImage(test_image, network)
            inputs.append(test_image)
            
            target_image = os.path.join(folder, 'masks', file)
            target_image = target_image[:-3]
            target_image = target_image + 'png'
            assert os.path.exists(target_image), "Can't find target image. Only png targets supported"
            target_image = cv2.imread(target_image, 0)
            target_image = cv2.resize(target_image, (network.IMAGE_WIDTH, network.IMAGE_HEIGHT))
            target_image = cv2.threshold(target_image, 127, 1, cv2.THRESH_BINARY)[1]
            targets.append(target_image)

            # Для многослойного выхода добавить classification target

        return inputs, targets

    def train_valid_test_split(self, X, ratio=None):
        if ratio is None:
            ratio = (0.7, .15, .15)

        N = len(X)
        return (
            X[:int(ceil(N * ratio[0]))],
            X[int(ceil(N * ratio[0])): int(ceil(N * ratio[0] + N * ratio[1]))],
            X[int(ceil(N * ratio[0] + N * ratio[1])):]
        )

    def num_batches_in_epoch(self):
        return int(math.floor(len(self.train_inputs) / self.batch_size))

    def reset_batch_pointer(self):
        # это должно перемешивать выборку, но кажется это не работает.
        permutation = np.random.permutation(len(self.train_inputs))
        self.train_inputs = [self.train_inputs[i] for i in permutation]
        self.train_targets = [self.train_targets[i] for i in permutation]

        self.pointer = 0

    def next_batch(self):
        inputs = []
        targets = []
        # print(self.batch_size, self.pointer, self.train_inputs.shape, self.train_targets.shape)
        for i in range(self.batch_size):
            inputs.append(np.array(self.train_inputs[self.pointer + i]))
            targets.append(np.array(self.train_targets[self.pointer + i]))

        self.pointer += self.batch_size

        return np.array(inputs, dtype=np.uint8), np.array(targets, dtype=np.uint8)

    @property
    def test_set(self):
        return np.array(self.test_inputs, dtype=np.uint8), np.array(self.test_targets, dtype=np.uint8)


def draw_results(test_inputs, test_targets, test_segmentation, test_cost, network, batch_num, n_examples_to_plot, path = "./"):
    fig, axs = plt.subplots(4, n_examples_to_plot, figsize=(n_examples_to_plot * 3, 10))
    fig.suptitle("Cost: {}".format(test_cost), fontsize=20)
    for example_i in range(n_examples_to_plot):
        axs[0][example_i].imshow(cv2.cvtColor(test_inputs[example_i], cv2.COLOR_BGR2RGB))
        axs[1][example_i].imshow(test_targets[example_i].astype(np.float32), cmap='gray', vmin=0, vmax=1)
        axs[2][example_i].imshow(np.reshape(test_segmentation[example_i], [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray', vmin=0, vmax=1)

        test_image_thresholded = np.array([0.0 if x < 0.5 else 1.0 for x in test_segmentation[example_i].flatten()])
        axs[3][example_i].imshow(np.reshape(test_image_thresholded, [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray', vmin=0, vmax=1)

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    IMAGE_PLOT_DIR = path + '/image_plots/'
    if not os.path.exists(IMAGE_PLOT_DIR):
        os.makedirs(IMAGE_PLOT_DIR)

    plt.savefig('{}/figure{}.jpg'.format(IMAGE_PLOT_DIR, batch_num))
    plt.clf() # TODO Правильно ли так удалять?

    return buf

def train(layers, 
          path,
          augmentation_seq = iaa.Sequential([]), 
          inputShape = [256, 320, 3],
          DataFolder = 'D:/CouchesTest/',
          LoadChckPntPath = None, 
          BatchSize = 2, 
          n_epochs = 300, 
          DEBUG = False, 
          CUDADevice = '0'):
    if DEBUG:
        print("-------------------->DEBUG MODE!<-------------------------")
        os.environ["CUDA_VISIBLE_DEVICES"] = '-1'
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = CUDADevice
    WritingPeriod = 1000 if not DEBUG else 10

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

    AllChckPntPath = os.path.join(path, "Checkpoints/save/", timestamp, 'all')
    BestChckPntPath = os.path.join(path, "Checkpoints/save/", timestamp, 'best') #Почему-то плохо записывает в эту папку

    # create directory for saving models
    os.makedirs(AllChckPntPath)
    os.makedirs(BestChckPntPath)

    BATCH_SIZE = BatchSize if not DEBUG else 1

    network = Network(layers, inputShape, True)
    dataset = Dataset(BATCH_SIZE, network, DataFolder)

    # не дает зашумлять маски (targetы)
    # change the activated augmenters for binary masks,
    # we only want to execute horizontal crop, flip and affine transformation
    def activator_binmasks(images, augmenter, parents, default):
        if augmenter.name in ["GaussianBlur", "Dropout", "GaussianNoise", "Adder", "Multiplier", "Contraster"]:
            return False
        else:
            # default value for all other augmenters
            return default

    hooks_binmasks = imgaug.HooksImages(activator=activator_binmasks) # не дает зашумлять маски (targetы)

    with tf.Session() as sess:
        saver = tf.train.Saver(tf.global_variables(), max_to_keep = 10)
        if LoadChckPntPath != None:
            ckpt = tf.train.get_checkpoint_state(LoadChckPntPath)
            if ckpt and ckpt.model_checkpoint_path:
                print('Restoring model: {}'.format(ckpt.model_checkpoint_path))
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                raise IOError('No model found in {}.'.format(LoadChckPntPath))
        else:
            sess.run(tf.global_variables_initializer())

        test_costs = []
        # Fit all training data
        global_start = time.time()
        for epoch_i in range(n_epochs):
            dataset.reset_batch_pointer()

            for batch_i in range(dataset.num_batches_in_epoch()):
                batch_num = epoch_i * dataset.num_batches_in_epoch() + batch_i + 1

                augmentation_seq_deterministic = augmentation_seq.to_deterministic()

                start = time.time()
                batch_inputs, batch_targets = dataset.next_batch()

                batch_inputs = np.reshape(batch_inputs, (dataset.batch_size, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS))
                batch_targets = np.reshape(batch_targets, (dataset.batch_size, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, 1))

                batch_inputs = augmentation_seq_deterministic.augment_images(batch_inputs)
                batch_targets = augmentation_seq_deterministic.augment_images(batch_targets, hooks=hooks_binmasks)

                cost, _ = sess.run([network.cost, network.train_op],
                                   feed_dict={network.inputs: batch_inputs, network.targets: batch_targets})
                end = time.time()
                print('{}/{}, epoch: {}, cost: {}, batch time: {}'.format(batch_num,
                                                                          n_epochs * dataset.num_batches_in_epoch(),
                                                                          epoch_i, cost, end - start))

                if batch_num % WritingPeriod == 0 or batch_num == n_epochs * dataset.num_batches_in_epoch():
                    test_inputs, test_targets = dataset.test_set
                    test_inputs, test_targets = test_inputs[:BATCH_SIZE], test_targets[:BATCH_SIZE]

                    test_inputs = np.reshape(test_inputs, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS))
                    test_targets = np.reshape(test_targets, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, 1))

                    test_cost = sess.run([network.cost],
                                                      feed_dict={network.inputs: test_inputs,
                                                                 network.targets: test_targets})

                    print('Step {}, test cost: {}'.format(batch_num, test_cost))
                    test_costs.append((test_cost, batch_num))
                    print("Costs in time: ", [test_costs[x][0] for x in range(len(test_costs))])
                    min_cost = min(test_costs)
                    print("Best cost: {} in batch {}".format(min_cost[0], min_cost[1]))
                    print("Total time: {}".format(time.time() - global_start))

                    # Plot example reconstructions
                    n_examples = 4 if not DEBUG else 4
                    test_inputs, test_targets = dataset.test_inputs[:n_examples], dataset.test_targets[:n_examples]
                    
                    test_segmentation = sess.run(network.segmentation_result, feed_dict={
                        network.inputs: np.reshape(test_inputs,
                                                   [n_examples, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS])})

                    # Prepare the plot
                    test_plot_buf = draw_results(test_inputs, test_targets, test_segmentation, test_cost, network,
                                                 batch_num, n_examples, path = path)

                    saver.save(sess, os.path.join(AllChckPntPath, 'model.ckpt'))
                    if test_cost <= min_cost[0]:
                        saver.save(sess, os.path.join(BestChckPntPath, 'model.ckpt'), global_step=batch_num)