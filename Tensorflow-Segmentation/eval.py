import time
t = time.time()

import os

import tensorflow as tf
import numpy as np
import cv2
import matplotlib.pyplot as plt

import sys
sys.path.append("../../")
import autoencoder

print('import time: {} sec'.format(time.time()-t))
t = time.time()

def list_files(in_path):
    files = []
    for (dirpath, dirnames, filenames) in os.walk(in_path):
        files.extend(filenames)
        break
    return files

def eval(layers, 
    path,
    inputShape,
    CheckpointPath,
    TestImagesPath = 'D:/TestInputs',
    CUDADevice = '-1'):
    t = time.time()

    os.environ["CUDA_VISIBLE_DEVICES"] = CUDADevice

    network = autoencoder.Network(layers, inputShape, False)

    print('network creation time: {} sec'.format(time.time()-t))
    t = time.time()

    OutputPath = os.path.join(path,"TestOutputs/")
    if not os.path.exists(OutputPath):
        os.makedirs(OutputPath)
    
    with tf.Session() as sess:
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(CheckpointPath)
        if ckpt and ckpt.model_checkpoint_path:
            print('Restoring model: {}'.format(ckpt.model_checkpoint_path))
            saver.restore(sess, ckpt.model_checkpoint_path)
        else:
            raise IOError('No model found in {}.'.format(SegmChckPntPath))

        print('restoring time: {} sec'.format(time.time()-t))

        files = list_files(TestImagesPath)
        for file in files:
            t = time.time()
            input_file = os.path.join(TestImagesPath,file)
            output_file_segm = os.path.join(OutputPath,"segm_" + file)

            image = np.array(cv2.imread(input_file, 1))
            image = autoencoder.PrepareImage(image, network)

            segmentation = sess.run(network.segmentation_result, feed_dict={network.inputs : np.reshape(image, [1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS])})

            fig, axs = plt.subplots( nrows=1, ncols=1 )
            axs.imshow(np.reshape(segmentation, [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray' , vmin=0, vmax=1 )
            fig.savefig(output_file_segm)   # save the figure to file
            plt.close(fig)    # close the figure

            print('segmentation time: {} sec'.format(time.time()-t))



