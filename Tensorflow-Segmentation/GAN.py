import tensorflow as tf
import numpy as np

from libs.activations import lrelu

from imgaug import augmenters as iaa

import os
os.environ["CUDA_VISIBLE_DEVICES"] = '0'

import queue
from threading import Thread

import random

import cv2

import time

IMAGE_HEIGHT = 128
IMAGE_WIDTH = 192
IMAGE_CHANNELS = 3

ConvLayersInfo = {}
def CreateConv(name, input, output_channels, kernel_size=3, strides=[1,1,1,1]):
    reuse = ConvLayersInfo.get(name) != None
    inputShape = input.get_shape()
    with tf.variable_scope(name, reuse = reuse):
        W = tf.get_variable("W", shape = (kernel_size, kernel_size, inputShape[3], output_channels))
        b = tf.Variable(tf.zeros([output_channels]))
    output = tf.nn.conv2d(input, W, strides = strides, padding = 'SAME')
    output = lrelu(tf.add(tf.contrib.layers.batch_norm(output), b))

    ConvLayersInfo[name] = (W, inputShape, strides)

    return output

def CreateDeconv(ProtoName, input):
    PrototypeInfo = ConvLayersInfo[ProtoName]
    OutputShape = PrototypeInfo[1]
    TensorShape = [tf.shape(input)[0], OutputShape[1], OutputShape[2], OutputShape[3]]
    TensorShape = tf.stack(TensorShape)
    output = tf.nn.conv2d_transpose(input, PrototypeInfo[0], TensorShape, PrototypeInfo[2], padding='SAME')
    output.set_shape(OutputShape)
    b = tf.Variable(tf.zeros([PrototypeInfo[0].get_shape().as_list()[2]]))
    output = lrelu(tf.add(tf.contrib.layers.batch_norm(output), b))
    return output

MaxPoolInfo = {}
def CreateMaxPool(name, input, kernel_size=2):
    inputShape = input.get_shape()
    ksize = (1, kernel_size, kernel_size, 1)

    MaxPoolInfo[name] = inputShape

    return tf.nn.max_pool(input, ksize = ksize, strides=ksize, padding='SAME')

def CreateUpsample(ProtoName, input):
    inputShape = MaxPoolInfo[ProtoName]
    output = tf.image.resize_nearest_neighbor(input, inputShape[1:3])
    output.set_shape((None, inputShape[1], inputShape[2], None))
    return output

FCInfo = {}
def CreateFC(name, input, output_channels):
    reuse = FCInfo.get(name) != None

    inputShape = input.get_shape()
    input = tf.reshape(input, [-1, int(inputShape[1] * inputShape[2] * inputShape[3])])

    output = tf.layers.dense(input, output_channels, name = name, reuse = reuse)
    output = tf.nn.sigmoid(output)

    FCInfo[name] = True

    return output

class Dataset:
    def __init__(self, batch_size, GenInputsPath, DiscInputsPath):
        self.augmentation_seq = iaa.Sequential([
            iaa.Add(value = (-45, 45), name="Adder"),
            iaa.Multiply(mul = (0.75, 1.25), name="Multiplier"),
            iaa.ContrastNormalization(alpha = (0.5, 1.5), name="Contraster"),

            iaa.Crop(px=(0, 40), name="Cropper"),  # crop images from each side by 0 to 16px (randomly chosen)
            iaa.Fliplr(0.5, name="Flipper")
            
            #iaa.Affine(scale={"x": (0.5, 1.5), "y": (0.5, 1.5)},translate_px={"x": (-32, 32), "y": (-32, 32)},rotate=(-45, 45),shear=(-32, 32),mode="constant",cval=0.0)
        ])
        self.que = queue.Queue()
        self.backgroundTask = None

        self.batch_size = batch_size

        self.GenInputs = os.listdir(GenInputsPath)
        self.GenInputs = [os.path.join(GenInputsPath, file) for file in self.GenInputs]
        
        self.DiscInputs = os.listdir(DiscInputsPath)
        self.DiscInputs = [os.path.join(DiscInputsPath, file) for file in self.DiscInputs]

    def file_path_to_image(self, file_name):
        image = np.array(cv2.imread(file_name, 1 if IMAGE_CHANNELS == 3 else 0))
        return image

    def num_batches_in_epoch(self):
        return int(math.floor(len(self.train_files) / self.batch_size))

    def __prepare_batch(self):
        GenInputs = []
        DiscInputs = []
        for i in range(self.batch_size):
            try:
                Path = random.choice(self.GenInputs)
                GenInput = self.file_path_to_image(Path)
                GenInput = np.array([GenInput], dtype=np.uint8)
                GenInput = self.aug_batch(GenInput)
                GenInput = Dataset.PrepareImage(GenInput[0,:,:,:])
                GenInputs.append(GenInput)
            except:
                print("Corrapted image: ", Path)
                self.GenInputs.remove(Path)
                return self.__prepare_batch()

            try:
                Path = random.choice(self.DiscInputs)
                DiscInput = self.file_path_to_image(Path)
                DiscInput = np.array([DiscInput], dtype=np.uint8)
                DiscInput = self.aug_batch(DiscInput)
                DiscInput = Dataset.PrepareImage(DiscInput[0,:,:,:])
                DiscInputs.append(DiscInput)
            except:
                print("Corrapted image: ", Path)
                self.DiscInputs.remove(Path)
                return self.__prepare_batch()

        GenInputs = np.array(GenInputs, dtype=np.float)
        DiscInputs = np.array(DiscInputs, dtype=np.float)

        return GenInputs, DiscInputs

    def aug_batch(self, batch_inputs):
        augmentation_seq_deterministic = self.augmentation_seq.to_deterministic()
        #batch_inputs = np.reshape(batch_inputs, (batch_inputs.shape[0], batch_inputs.shape[1], batch_inputs.shape[2], IMAGE_CHANNELS))
        batch_inputs = augmentation_seq_deterministic.augment_images(batch_inputs)
        return batch_inputs

    def next_batch(self):
        if self.backgroundTask == None:
            GenInputs, DiscInputs = self.__prepare_batch()
        else:
            t = time.time()
            self.backgroundTask.join()
            GenInputs, DiscInputs = self.que.get()
            t = time.time() - t
            if t > 0.01:
                print("GPU was waiting data for {} secs".format(t))
        
        self.backgroundTask = Thread(target=lambda q: q.put(self.__prepare_batch()), args=(self.que,))
        self.backgroundTask.start()
        return GenInputs, DiscInputs

    @property
    def test_set(self):
        # print(self.batch_size, self.pointer, self.train_inputs.shape, self.train_targets.shape)
        for file in self.test_files:
            yield self.file_path_to_image(file)

    @staticmethod
    def PrepareImage(test_image):
        # Добавить поворот вертикальных изображений
        test_image = cv2.resize(test_image, (IMAGE_WIDTH, IMAGE_HEIGHT))
        test_image = np.array(test_image, dtype = np.float)
        test_image -= 128.0
        test_image /= 128.0
        return test_image

    @staticmethod
    def RestoreImage(test_image):
        test_image *= 128.0
        test_image += 128.0
        test_image = np.clip(test_image, 0.0 , 255.0)
        test_image = np.array(test_image, dtype = np.uint8)
        return test_image

GenInput = tf.placeholder(tf.float32, [None, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS])
output = CreateConv("GC0", GenInput, 64)
output = CreateConv("GC1", output, 64)
output = CreateMaxPool("GMP2", output)
output = CreateConv("GC3", output, 128)
output = CreateConv("GC4", output, 128)
output = CreateMaxPool("GMP5", output)
output = CreateConv("GC6", output, 256)
output = CreateConv("GC7", output, 256)
output = CreateConv("GC8", output, 256)
output = CreateConv("GC9", output, 256)
output = CreateMaxPool("GMP10", output)
output = CreateConv("GC11", output, 512)
output = CreateConv("GC12", output, 512)
output = CreateConv("GC13", output, 512)
output = CreateConv("GC14", output, 512)
output = CreateMaxPool("GMP15", output)
output = CreateConv("GC16", output, 512)
output = CreateConv("GC17", output, 512)
output = CreateConv("GC18", output, 512)
output = CreateConv("GC19", output, 512)
output = CreateMaxPool("GMP20", output)
output = CreateUpsample("GMP20", output)
output = CreateDeconv("GC19", output)
output = CreateDeconv("GC18", output)
output = CreateDeconv("GC17", output)
output = CreateDeconv("GC16", output)
output = CreateUpsample("GMP15", output)
output = CreateDeconv("GC14", output)
output = CreateDeconv("GC13", output)
output = CreateDeconv("GC12", output)
output = CreateDeconv("GC11", output)
output = CreateUpsample("GMP10", output)
output = CreateDeconv("GC9", output)
output = CreateDeconv("GC8", output)
output = CreateDeconv("GC7", output)
output = CreateDeconv("GC6", output)
output = CreateUpsample("GMP5", output)
output = CreateDeconv("GC4", output)
output = CreateDeconv("GC3", output)
output = CreateUpsample("GMP2", output)
output = CreateDeconv("GC1", output)
GenOutput = CreateDeconv("GC0", output)
print("Generator input shape: {}, OutputShape: {}.".format(GenInput.get_shape(), GenOutput.get_shape()))

GenVars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)

DiscInput = tf.placeholder(tf.float32, [None, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS])
output = CreateConv("DC0", DiscInput, 64)
output = CreateConv("DC1", output, 64)
output = CreateMaxPool("DMP2", output)
output = CreateConv("DC3", output, 128)
output = CreateConv("DC4", output, 128)
output = CreateMaxPool("DMP5", output)
output = CreateConv("DC6", output, 256)
output = CreateConv("DC7", output, 256)
output = CreateConv("DC8", output, 256)
output = CreateConv("DC9", output, 256)
output = CreateMaxPool("DMP10", output)
output = CreateConv("DC11", output, 512)
output = CreateConv("DC12", output, 512)
output = CreateConv("DC13", output, 512)
output = CreateConv("DC14", output, 512)
output = CreateMaxPool("DMP15", output)
output = CreateConv("DC16", output, 512)
output = CreateConv("DC17", output, 512)
output = CreateConv("DC18", output, 512)
output = CreateConv("DC19", output, 512)
output = CreateMaxPool("DMP20", output)
DiscRealOutput = CreateFC("DFC21", output,1)

output = CreateConv("DC0", GenOutput, 64)
output = CreateConv("DC1", output, 64)
output = CreateMaxPool("DMP2", output)
output = CreateConv("DC3", output, 128)
output = CreateConv("DC4", output, 128)
output = CreateMaxPool("DMP5", output)
output = CreateConv("DC6", output, 256)
output = CreateConv("DC7", output, 256)
output = CreateConv("DC8", output, 256)
output = CreateConv("DC9", output, 256)
output = CreateMaxPool("DMP10", output)
output = CreateConv("DC11", output, 512)
output = CreateConv("DC12", output, 512)
output = CreateConv("DC13", output, 512)
output = CreateConv("DC14", output, 512)
output = CreateMaxPool("DMP15", output)
output = CreateConv("DC16", output, 512)
output = CreateConv("DC17", output, 512)
output = CreateConv("DC18", output, 512)
output = CreateConv("DC19", output, 512)
output = CreateMaxPool("DMP20", output)
DiscFakeOutput = CreateFC("DFC21", output,1)

GenLoss = -tf.reduce_mean(tf.log(DiscFakeOutput))
DiscLoss = -tf.reduce_mean(tf.log(DiscRealOutput) + tf.log(1.0 - DiscFakeOutput))

DiscVars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
for var in GenVars:
    DiscVars.remove(var)

GenOptimizer = tf.train.AdamOptimizer(0.0001).minimize(GenLoss, var_list = GenVars)
DiscOptimizer = tf.train.AdamOptimizer(0.0001).minimize(DiscLoss, var_list = DiscVars)

d = Dataset(1, "F:/Hard/", "F:/Simple/")

PreTrainLoss = tf.reduce_mean(tf.abs(GenInput - GenOutput))
PreTrainOptimizer = tf.train.AdamOptimizer(0.0001).minimize(PreTrainLoss, var_list = GenVars)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    
    for i in range(10):
        gIN, dIN = d.next_batch()
        loss, gin, gout,_ = sess.run((PreTrainLoss, GenInput, GenOutput, PreTrainOptimizer), { GenInput : gIN})
        print("PreTrainLoss: {}".format(loss))
        
    for i in range(10):
        gIN, dIN = d.next_batch()
        resIn, resOut = sess.run((GenInput, GenOutput), { GenInput : gIN})
        cv2.imwrite("D:/Temp/GAN/{}i.jpg".format(i), Dataset.RestoreImage(resIn[0]))
        cv2.imwrite("D:/Temp/GAN/{}o.jpg".format(i), Dataset.RestoreImage(resOut[0]))

    for i in range(10):
        gIN, dIN = d.next_batch()
        genL, discL, discF, discR,_,_ = sess.run((GenLoss, DiscLoss, DiscFakeOutput, DiscRealOutput, GenOptimizer, DiscOptimizer), { GenInput : gIN, DiscInput:dIN})
        print("GenLoss: {}, DiscLoss: {}, DiscFakeOut: {}, DiscRealOut: {}".format(genL, discL, discF, discR))
        
    for i in range(10):
        gIN, dIN = d.next_batch()
        resIn, resOut = sess.run((GenInput, GenOutput), { GenInput : gIN})
        cv2.imwrite("D:/Temp/GAN/{}i.jpg".format(i), Dataset.RestoreImage(resIn[0]))
        cv2.imwrite("D:/Temp/GAN/{}o.jpg".format(i), Dataset.RestoreImage(resOut[0]))

    saver = tf.train.Saver()
    saver.save(sess, "D:/Temp/GAN/chckpnt/model.ckpt")

print("Hello world")