import math
import os
import time
from math import ceil

import cv2
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from imgaug import augmenters as iaa
from imgaug import imgaug
from conv2d import Conv2d
from libs.activations import lrelu
from libs.activations import linear
import datetime
import io

import queue
from threading import Thread

class Network:
    def __init__(self, layers, inputShape = [256, 320, 3], trainable = False, classNumber = 9):
        self.IMAGE_HEIGHT = inputShape[0]
        self.IMAGE_WIDTH = inputShape[1]
        self.IMAGE_CHANNELS = inputShape[2]
        self.CLASS_NUMBER = classNumber

        self.inputs = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.IMAGE_CHANNELS], name='inputs')
        self.classTarget = tf.placeholder(tf.float32, [None, self.CLASS_NUMBER], name='targets')
        self.layers = {}

        # благодаря этой строчке не нужно вычитать средный пиксель из каждого фото
        #list_of_images_norm = tf.map_fn(tf.image.per_image_standardization, self.inputs)
        #net = tf.stack(list_of_images_norm)
        # но лучшие результаты получаются если не масштабировать:
        net = self.inputs - tf.reduce_mean(self.inputs)
        print("Current input shape: ", net.get_shape())

        # ENCODER
        for layer in layers:
            self.layers[layer.name] = net = layer.create_layer(net)
            
        self.ENCODER_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)

        self.DecoderInput = net

        # Слой классификатора
        classNet = tf.reshape(net, [-1, int(net.get_shape()[1]) * int(net.get_shape()[2]) * int(net.get_shape()[3])])
        classNet = tf.layers.dense(classNet, self.CLASS_NUMBER)
        self.classResult = tf.nn.sigmoid(classNet)

        self.CLASS_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
        for var in self.ENCODER_vars:
            self.CLASS_vars.remove(var)

        print("shapes: res: {} vs targ: {}".format(self.classResult.get_shape(), self.classTarget.get_shape()))

        if trainable:
            self.classCost = tf.reduce_sum(tf.square(self.classResult - self.classTarget))
            self.classTrain_op = tf.train.AdamOptimizer(0.0001).minimize(self.classCost)


    def Reverse(self, layers, trainable = False, train_all = True):
        self.targets = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, 1], name='targets')
        layers.reverse()

        # ПОДМЕНА СЛОЯ!!!!!
        layers.pop()
        LastLayer = Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_0_0')
        layers.append(LastLayer)
        temp = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, 1], name='inputs2') #убрать этот placeholder
        self.layers[LastLayer.name] = LastLayer.create_layer(temp)
        # ПОДМЕНА СЛОЯ!!!!!
        
        Conv2d.reverse_global_variables()

        net = self.DecoderInput

        # DECODER
        for layer in layers:
            net = layer.create_layer_reversed(net, prev_layer=self.layers[layer.name], actFunc = lrelu if layer.name != 'conv_0_0' else linear, shared_weights = train_all)

            
        self.DECODER_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
        for var in self.ENCODER_vars:
            self.DECODER_vars.remove(var)
        for var in self.CLASS_vars:
            self.DECODER_vars.remove(var)

        self.segmentation_result = tf.sigmoid(net)
        
        print("shapes: res: {} vs targ: {}".format(self.segmentation_result.get_shape(), self.targets.get_shape()))

        if trainable:
            # MSE loss
            self.cost = tf.sqrt(tf.reduce_sum(tf.square(self.segmentation_result - self.targets)))
            if train_all:
                self.train_op = tf.train.AdamOptimizer(name = "SegmAdam" ).minimize(self.cost)
            else:
                self.train_op = tf.train.AdamOptimizer(name = "SegmAdam" ).minimize(self.cost, var_list=self.DECODER_vars)

class Dataset:

    def __init__(self, batch_size, network, folder, augmentation_seq):
        self.augmentation_seq = augmentation_seq
        self.hooks_binmasks = imgaug.HooksImages(activator=self.__activator_binmasks) # не дает зашумлять маски (targetы)
        self.que = queue.Queue()
        self.backgroundTask = None

        self.IMAGE_HEIGHT = network.IMAGE_HEIGHT
        self.IMAGE_WIDTH = network.IMAGE_WIDTH
        self.IMAGE_CHANNELS = network.IMAGE_CHANNELS
        self.CLASS_NUMBER = network.CLASS_NUMBER

        self.batch_size = batch_size
        self.network = network
        self.folder = folder

        self.train_files, self.test_files = self.__train_valid_test_split(os.listdir(os.path.join(folder, 'inputs')))

        self.pointer = 0

    def file_path_to_image(self, file_name):
        folder = self.folder
        network = self.network
        file = file_name
        input_image = os.path.join(folder, 'inputs', file)
        test_image = np.array(cv2.imread(input_image, 1))
        assert test_image.shape[0] <= test_image.shape[1], "Only horizontal images supported. Check this file: {}".format(input_image)
        test_image = Dataset.PrepareImage(test_image, network)
        
        target_path = os.path.join(folder, 'masks', file)
        target_path = target_path[:-3]
        target_path = target_path + 'png'
        target_image = self.__ReadMask_fromPNG(target_path)

        target_class = self.__read_classes(input_image)

        return test_image, target_image, target_class

    def __train_valid_test_split(self, X, ratio=None):
        if ratio is None:
            ratio = (0.9, .1)

        N = len(X)
        return (
            X[:int(ceil(N * ratio[0]))],
            X[int(ceil(N * ratio[0])):]
        )

    def num_batches_in_epoch(self):
        return int(math.floor(len(self.train_files) / self.batch_size))

    def reset_batch_pointer(self):
        self.pointer = 0

        permutation = np.random.permutation(len(self.train_files))
        self.train_files = [self.train_files[i] for i in permutation]
        permutation = np.random.permutation(len(self.test_files))
        self.test_files = [self.test_files[i] for i in permutation]

    def __prepare_batch(self, pointer):
        inputs = []
        targets = []
        classes = []
        if pointer + self.batch_size - 1 >= len(self.train_files):
            pointer -= self.batch_size
        for i in range(self.batch_size):
            input, mask, Class = self.file_path_to_image(self.train_files[pointer + i])
            inputs.append(input)
            targets.append(mask)
            classes.append(Class)

        batch_inputs = np.array(inputs, dtype=np.uint8)
        batch_targets = np.array(targets, dtype=np.uint8)
        batch_classes = np.array(classes, dtype=np.uint8)

        batch_inputs, batch_targets = self.aug_batch(batch_inputs, batch_targets, self.batch_size)

        return batch_inputs, batch_targets, batch_classes

    def aug_batch(self, batch_inputs, batch_targets, size):
        augmentation_seq_deterministic = self.augmentation_seq.to_deterministic()

        batch_inputs = np.reshape(batch_inputs, (size, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.IMAGE_CHANNELS))
        batch_targets = np.reshape(batch_targets, (size, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, 1))

        batch_inputs = augmentation_seq_deterministic.augment_images(batch_inputs)
        batch_targets = augmentation_seq_deterministic.augment_images(batch_targets, hooks=self.hooks_binmasks)
        
        return batch_inputs, batch_targets

    def next_batch(self):

        if self.backgroundTask == None:
            batch_inputs, batch_targets, batch_classes = self.__prepare_batch(self.pointer)
            self.pointer += self.batch_size
        else:
            t = time.time()
            self.backgroundTask.join()
            batch_inputs, batch_targets, batch_classes = self.que.get()
            t = time.time() - t
            if t > 0.01:
                print("GPU was waiting data for {} secs".format(t))

        self.backgroundTask = Thread(target=lambda q, arg1: q.put(self.__prepare_batch(arg1)), args=(self.que, self.pointer))
        self.pointer += self.batch_size
        self.backgroundTask.start()

        return batch_inputs, batch_targets, batch_classes

    @property
    def test_set(self):
        # print(self.batch_size, self.pointer, self.train_inputs.shape, self.train_targets.shape)
        for file in self.test_files:
            yield self.file_path_to_image(file)

    # не дает зашумлять маски (targetы)
    # change the activated augmenters for binary masks,
    # we only want to execute horizontal crop, flip and affine transformation
    def __activator_binmasks(self, images, augmenter, parents, default):
        if augmenter.name in ["GaussianBlur", "Dropout", "GaussianNoise", "Adder", "Multiplier", "Contraster"]:
            return False
        else:
            # default value for all other augmenters
            return default

    # Преобразует  путь к файлу в вектор классов
    def __read_classes(self, path):
        print("WARNING! unimplemented function __read_classes")
        return np.ones((self.CLASS_NUMBER))

    def __ReadMask_fromPNG(self, path):
        assert os.path.exists(path), "Can't find target image at {}. Only png targets supported".format(path)
        target_image = cv2.imread(path, 0)
        target_image = cv2.resize(target_image, (self.IMAGE_WIDTH, self.IMAGE_HEIGHT))
        target_image = cv2.threshold(target_image, 127, 1, cv2.THRESH_BINARY)[1]
        return target_image

    # Read mask in compressed format [H * W * 1] bytes
    def __ReadMask(self, path):
        mask=np.zeros((256,320),dtype=bytes)
        path_parts=os.path.split(path)
        if path_parts[1][0]=='0':
            fromfile=np.load(path)
            for i in range(len(fromfile)):
                for j in range(len(fromfile[0])):
                    mask[i,j]=int(fromfile[i,j])
        else:
            with open(path, "rb") as file0:
                for i in range(256):
                    for j in range(320):
                        a=int.from_bytes(file0.read(1),byteorder='big')
                        mask[i,j]=a
        return mask

    # Make full mask from compressed mask
    def __MakeMultiMask(self, input_mask):
        mask=np.zeros((256,320,9),dtype=bool)
        for i in range(256):
            for j in range(320):
                a=int(input_mask[i,j])
                mask[i,j,a]=True
        return mask    

    # Make full mask from file
    def __ParseMask(self, path):
        mask=np.zeros((256,320,9),dtype=bool)
        path_parts=os.path.split(path)
        if path_parts[1][0]=='0':
            fromfile=np.load(path)
            for i in range(len(fromfile)):
                for j in range(len(fromfile[0])):
                    mask[i,j,int(fromfile[i,j])]=True
        else:
            with open(path, "rb") as file0:
                for i in range(256):
                    for j in range(320):
                        a=int.from_bytes(file0.read(1),byteorder='big')
                        mask[i,j,a]=True
        return mask

    def __MasksToImages(self, multimasks):
        Nim=len(multimasks)
        H=len(multimasks[0])
        W=len(multimasks[0][0])
        Ncat=len(multimasks[0][0,0])
        for l in range(Nim):
            im=np.zeros((H,W),dtype=np.uint8)
            for i in range(len(im)):
                for j in range(len(im[0])):
                    for k in range(Ncat):
                        if multimasks[l][i,j,k]:
                            im[i,j]=k*255/8
                            break   
            scipy.misc.imsave(str(l)+'.png', im)

    @staticmethod
    def PrepareImage(test_image, network):
        # Добавить поворот вертикальных изображений
        return cv2.resize(test_image, (network.IMAGE_WIDTH, network.IMAGE_HEIGHT))

def draw_results(test_inputs, test_targets, test_segmentation, test_cost, network, batch_num, n_examples_to_plot, path = "./"):
    fig, axs = plt.subplots(4, n_examples_to_plot, figsize=(n_examples_to_plot * 3, 10))
    fig.suptitle("Cost: {}".format(test_cost), fontsize=20)
    for example_i in range(n_examples_to_plot):
        axs[0][example_i].imshow(cv2.cvtColor(np.reshape(test_inputs[example_i], [network.IMAGE_HEIGHT, network.IMAGE_WIDTH, 3]), cv2.COLOR_BGR2RGB))
        axs[1][example_i].imshow(np.reshape(test_targets[example_i], [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]).astype(np.float32), cmap='gray', vmin=0, vmax=1)
        axs[2][example_i].imshow(np.reshape(test_segmentation[example_i], [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray', vmin=0, vmax=1)

        test_image_thresholded = np.array([0.0 if x < 0.5 else 1.0 for x in test_segmentation[example_i].flatten()])
        axs[3][example_i].imshow(np.reshape(test_image_thresholded, [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray', vmin=0, vmax=1)

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    IMAGE_PLOT_DIR = path + '/image_plots/'
    if not os.path.exists(IMAGE_PLOT_DIR):
        os.makedirs(IMAGE_PLOT_DIR)

    plt.savefig('{}/figure{}.jpg'.format(IMAGE_PLOT_DIR, batch_num))
    plt.clf() # TODO Правильно ли так удалять?

    return buf

def TrainEncoder(layers, 
          path,
          augmentation_seq = iaa.Sequential([]), 
          inputShape = [256, 320, 3],
          DataFolder = 'D:/CouchesTest/',
          LoadEncoderPath = None, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 2, 
          n_epochs = 300, 
          CUDADevice = '0',
          WritingPeriod = 10000,
          SavingPath = None,
          TrainEncoder = True,
          TrainDecoder = True,
          TrainClass = True,
          ClassNumber = 9,
          PlotExamples = 12):
    os.environ["CUDA_VISIBLE_DEVICES"] = CUDADevice

    if not TrainDecoder: PlotExamples = 0

    if SavingPath == None:
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
        SavingPath = os.path.join(path, "save", timestamp)
        
    SaveBestEncoderPath = os.path.join(SavingPath, 'best', 'encoder')
    SaveBestClassPath = os.path.join(SavingPath, 'best', 'class')
    SaveBestDecoderPath = os.path.join(SavingPath, 'best', 'decoder')
    SaveLastEncoderPath = os.path.join(SavingPath, 'last', 'encoder')
    SaveLastClassPath = os.path.join(SavingPath, 'last', 'class')
    SaveLastDecoderPath = os.path.join(SavingPath, 'last', 'decoder')
    if not os.path.exists(SaveBestEncoderPath): os.makedirs(SaveBestEncoderPath)
    if not os.path.exists(SaveBestClassPath): os.makedirs(SaveBestClassPath)
    if not os.path.exists(SaveBestDecoderPath): os.makedirs(SaveBestDecoderPath)
    if not os.path.exists(SaveLastEncoderPath): os.makedirs(SaveLastEncoderPath)
    if not os.path.exists(SaveLastClassPath): os.makedirs(SaveLastClassPath)
    if not os.path.exists(SaveLastDecoderPath): os.makedirs(SaveLastDecoderPath)

    with tf.Graph().as_default():
        network = Network(layers, inputShape, True, ClassNumber)
        dataset = Dataset(BATCH_SIZE, network, DataFolder, augmentation_seq)

        with tf.Session() as sess:
            BestEncoderSaver = tf.train.Saver(network.ENCODER_vars, max_to_keep = 3)
            BestClassSaver = tf.train.Saver(network.CLASS_vars, max_to_keep = 3)
            LastEncoderSaver = tf.train.Saver(network.ENCODER_vars, max_to_keep = 1)
            LastClassSaver = tf.train.Saver(network.CLASS_vars, max_to_keep = 1)
            if LoadEncoderPath:
                ckpt = tf.train.get_checkpoint_state(LoadEncoderPath)
                if ckpt and ckpt.model_checkpoint_path:
                    print('Restoring encoder: {}'.format(ckpt.model_checkpoint_path))
                    LastEncoderSaver.restore(sess, ckpt.model_checkpoint_path)
                else:
                    raise IOError('No model found in {}.'.format(LoadChckPntPath))
            else:
                sess.run(tf.variables_initializer(network.ENCODER_vars))
            if LoadClassPath:
                ckpt = tf.train.get_checkpoint_state(LoadClassPath)
                if ckpt and ckpt.model_checkpoint_path:
                    print('Restoring classifier: {}'.format(ckpt.model_checkpoint_path))
                    LastClassSaver.restore(sess, ckpt.model_checkpoint_path)
                else:
                    raise IOError('No model found in {}.'.format(LoadChckPntPath))
            else:
                sess.run(tf.variables_initializer(network.CLASS_vars))

            shared_weights = False if (TrainEncoder == False and TrainDecoder == True) else True
            network.Reverse(layers, True, shared_weights) # not nessesary build decoder when we train classifier only

            print("DEBUG!")
            print("encoder vars:")
            print(network.ENCODER_vars)
            print("class vars:")
            print(network.CLASS_vars)
            print("decoder vars:")
            print(network.DECODER_vars)
            print("DEBUG!")

            BestDecoderSaver = tf.train.Saver(network.DECODER_vars, max_to_keep = 3)
            LastDecoderSaver = tf.train.Saver(network.DECODER_vars, max_to_keep = 1)
            if LoadDecoderPath:
                ckpt = tf.train.get_checkpoint_state(LoadDecoderPath)
                if ckpt and ckpt.model_checkpoint_path:
                    print('Restoring decoder: {}'.format(ckpt.model_checkpoint_path))
                    LastDecoderSaver.restore(sess, ckpt.model_checkpoint_path)
                else:
                    raise IOError('No model found in {}.'.format(LoadChckPntPath))
            else:
                sess.run(tf.variables_initializer(network.DECODER_vars))

            other_vars = tf.global_variables() # initializing not savable vars

            for var in network.ENCODER_vars:
                other_vars.remove(var)
            for var in network.CLASS_vars:
                other_vars.remove(var)
            for var in network.DECODER_vars:
                other_vars.remove(var)
            sess.run(tf.variables_initializer(other_vars))

            test_costs = []
            # Fit all training data
            global_start = time.time()
            for epoch_i in range(n_epochs):
                dataset.reset_batch_pointer()

                for batch_i in range(dataset.num_batches_in_epoch()):
                    batch_num = epoch_i * dataset.num_batches_in_epoch() + batch_i + 1


                    start = time.time()
                    batch_inputs, batch_targets, batch_classes = dataset.next_batch()

                    fetches = []
                    dict = {}
                    dict[network.inputs] = batch_inputs
                    if TrainClass:
                        fetches.append(network.classCost)
                        fetches.append(network.classTrain_op)
                        dict[network.classTarget] = batch_classes
                    if TrainDecoder:
                        fetches.append(network.cost)
                        fetches.append(network.train_op)
                        dict[network.targets] = batch_targets

                    result = sess.run(fetches, dict)

                    if TrainClass:
                        classCost = result[0]
                        cost = result[2] if TrainDecoder else None
                    else:
                        classCost = None
                        cost = result[0] if TrainDecoder else None

                    end = time.time()
                    print('{}/{}, epoch: {}, cost: {}, classification cost: {}, batch time: {}'.format(batch_num,
                                                                              n_epochs * dataset.num_batches_in_epoch(),
                                                                              epoch_i, cost, classCost, end - start))

                    if batch_num % WritingPeriod == 0 or batch_num == n_epochs * dataset.num_batches_in_epoch():
                        testTime = time.time()
                        print("Start testing")
                        n = 0
                        test_cost = 0
                        test_class_cost = 0

                        plot_inputs = []
                        plot_targets = []
                        plot_results = []

                        for test_input, test_target, test_class in dataset.test_set:
                            test_input = np.reshape(test_input, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS))
                            test_target = np.reshape(test_target, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, 1))
                            test_class = np.reshape(test_class, (-1, network.CLASS_NUMBER))

                            n = n + 1

                            fetches = []
                            dict = {}
                            dict[network.inputs] = test_input
                            if TrainClass:
                                fetches.append(network.classCost)
                                dict[network.classTarget] = test_class
                            if TrainDecoder:
                                fetches.append(network.cost)
                                fetches.append(network.segmentation_result)
                                dict[network.targets] = test_target

                            result = sess.run(fetches, dict)

                            if TrainClass:
                                test_class_cost += result[0]
                                test_cost += result[1] if TrainDecoder else 0
                                test_segmentation = result[2] if TrainDecoder else None
                            else:
                                test_class_cost += 0
                                test_cost += result[0] if TrainDecoder else 0
                                test_segmentation = result[1] if TrainDecoder else None
                            
                            if len(plot_results) < PlotExamples:
                                plot_inputs.append(test_input)
                                plot_targets.append(test_target)
                                plot_results.append(test_segmentation)

                        print("Testing finished in {} secs".format(time.time()-testTime))
                        test_class_cost /= n
                        test_cost /= n

                        print('Step {}, test cost: {}, test classification cost: {}'.format(batch_num, test_cost, test_class_cost))
                        if not TrainDecoder: test_cost = test_class_cost
                        test_costs.append((test_cost, batch_num))
                        print("Costs in time: ", [test_costs[x][0] for x in range(len(test_costs))])
                        min_cost = min(test_costs)
                        print("Best cost: {} in batch {}".format(min_cost[0], min_cost[1]))
                        print("Total time: {}".format(time.time() - global_start))

                        if TrainDecoder:
                            test_plot_buf = draw_results(plot_inputs, plot_targets, plot_results, test_cost, network,
                                                     batch_num, len(plot_results), path = SavingPath)

                        LastEncoderSaver.save(sess, os.path.join(SaveLastEncoderPath, 'model.ckpt'), global_step=batch_num)
                        LastClassSaver.save(sess, os.path.join(SaveLastClassPath, 'model.ckpt'), global_step=batch_num)
                        LastDecoderSaver.save(sess, os.path.join(SaveLastDecoderPath, 'model.ckpt'), global_step=batch_num)
                        if test_cost <= min_cost[0]:
                            BestEncoderSaver.save(sess, os.path.join(SaveBestEncoderPath, 'model.ckpt'), global_step=batch_num)
                            BestClassSaver.save(sess, os.path.join(SaveBestClassPath, 'model.ckpt'), global_step=batch_num)
                            BestDecoderSaver.save(sess, os.path.join(SaveBestDecoderPath, 'model.ckpt'), global_step=batch_num)
    return 1
