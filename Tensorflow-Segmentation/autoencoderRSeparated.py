import math
import os
import time
from math import ceil

import cv2
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from imgaug import augmenters as iaa
from imgaug import imgaug
from conv2d import Conv2d
from libs.activations import lrelu
from libs.activations import linear
import datetime
import io

import queue
from threading import Thread

class Network:
    def __init__(self, layers, inputShape = [256, 320, 3], trainable = False, classNumber = 9):
        self.IMAGE_HEIGHT = inputShape[0]
        self.IMAGE_WIDTH = inputShape[1]
        self.IMAGE_CHANNELS = inputShape[2]
        self.CLASS_NUMBER = classNumber

        self.inputs = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.IMAGE_CHANNELS], name='inputs')
        self.targets = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.CLASS_NUMBER], name='targets')

        self.layers = {}

        # благодаря этой строчке не нужно вычитать средный пиксель из каждого фото
        #list_of_images_norm = tf.map_fn(tf.image.per_image_standardization, self.inputs)
        #net = tf.stack(list_of_images_norm)
        # но лучшие результаты получаются если не масштабировать:
        net = self.inputs - tf.reduce_mean(self.inputs)
        print("Current input shape: ", net.get_shape())

        # ENCODER
        for layer in layers:
            self.layers[layer.name] = net = layer.create_layer(net)

        self.DecoderInput = net

        # Слой классификатора
        classNet = tf.reshape(net, [-1, int(net.get_shape()[1]) * int(net.get_shape()[2]) * int(net.get_shape()[3])])
        classNet = tf.layers.dense(classNet, self.CLASS_NUMBER)
        self.classResult = tf.nn.sigmoid(classNet)
        
        self.classTarget = tf.reduce_max(self.targets, [1,2])

        if trainable:
            self.classCost = tf.reduce_mean(tf.square(self.classResult - self.classTarget))
            self.classTrain_op = tf.train.AdamOptimizer(0.0001).minimize(self.classCost)

    def Reverse(self, layers, trainable = False):
        layers.reverse()

        # ПОДМЕНА СЛОЯ!!!!!
        layers.pop()
        LastLayer = Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_0_0')
        layers.append(LastLayer)
        temp = tf.placeholder(tf.float32, [None, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.CLASS_NUMBER], name='inputs2') #убрать этот placeholder
        self.layers[LastLayer.name] = LastLayer.create_layer(temp)
        # ПОДМЕНА СЛОЯ!!!!!
        
        Conv2d.reverse_global_variables()

        net = self.DecoderInput

        # DECODER
        for layer in layers:
            net = layer.create_layer_reversed(net, prev_layer=self.layers[layer.name], actFunc = lrelu if layer.name != 'conv_0_0' else linear)
        #self.segmentation_result = tf.sigmoid(net)
        self.segmentation_result = tf.nn.softmax(net, 3)

        print('segmentation_result.shape: {}, targets.shape: {}'.format(self.segmentation_result.get_shape(), self.targets.get_shape()))

        
        if trainable:
            # MSE loss
            self.cost = tf.sqrt(tf.reduce_mean(tf.square(self.segmentation_result - self.targets)))
            self.train_op = tf.train.AdamOptimizer(name = "SegmAdam" ).minimize(self.cost)

class Dataset:

    def __init__(self, batch_size, network, folder, augmentation_seq):
        self.augmentation_seq = augmentation_seq
        self.hooks_binmasks = imgaug.HooksImages(activator=self.__activator_binmasks) # не дает зашумлять маски (targetы)
        self.que = queue.Queue()
        self.backgroundTask = None

        self.IMAGE_HEIGHT = network.IMAGE_HEIGHT
        self.IMAGE_WIDTH = network.IMAGE_WIDTH
        self.IMAGE_CHANNELS = network.IMAGE_CHANNELS
        self.CLASS_NUMBER = network.CLASS_NUMBER

        self.batch_size = batch_size
        self.network = network
        self.folder = folder

        self.train_files, self.test_files = self.__train_valid_test_split(os.listdir(os.path.join(folder, 'inputs')))

        self.pointer = 0

    def file_path_to_image(self, file_name):
        folder = self.folder
        network = self.network
        file = file_name
        input_image = os.path.join(folder, 'inputs', file)
        test_image = np.array(cv2.imread(input_image, 1))
        assert test_image.shape[0] <= test_image.shape[1], "Only horizontal images supported. Check this file: {}".format(input_image)
        test_image = Dataset.PrepareImage(test_image, network)

        mask_file=file[:-4]+'_mask'
        target_image = self.__ParseMask(os.path.join(folder, 'masks', mask_file))

        return test_image, target_image

    def __train_valid_test_split(self, X, ratio=None):
        if ratio is None:
            ratio = (0.9, .1)

        N = len(X)
        return (
            X[:int(ceil(N * ratio[0]))],
            X[int(ceil(N * ratio[0])):]
        )

    def num_batches_in_epoch(self):
        return int(math.floor(len(self.train_files) / self.batch_size))

    def reset_batch_pointer(self):
        self.pointer = 0

        permutation = np.random.permutation(len(self.train_files))
        self.train_files = [self.train_files[i] for i in permutation]
        permutation = np.random.permutation(len(self.test_files))
        self.test_files = [self.test_files[i] for i in permutation]

    def __prepare_batch(self, pointer):
        inputs = []
        targets = []
        if pointer + self.batch_size - 1 >= len(self.train_files):
            pointer -= self.batch_size
        for i in range(self.batch_size):
            input, mask = self.file_path_to_image(self.train_files[pointer + i])
            inputs.append(input)
            targets.append(mask)

        batch_inputs = np.array(inputs, dtype=np.uint8)
        batch_targets = np.array(targets, dtype=np.uint8)

        return self.aug_batch(batch_inputs, batch_targets, self.batch_size)

    def aug_batch(self, batch_inputs, batch_targets, size):
        augmentation_seq_deterministic = self.augmentation_seq.to_deterministic()

        batch_inputs = np.reshape(batch_inputs, (size, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.IMAGE_CHANNELS))
        batch_targets = np.reshape(batch_targets, (size, self.IMAGE_HEIGHT, self.IMAGE_WIDTH, self.CLASS_NUMBER))

        batch_inputs = augmentation_seq_deterministic.augment_images(batch_inputs)
        batch_targets = augmentation_seq_deterministic.augment_images(batch_targets, hooks=self.hooks_binmasks)
        
        return batch_inputs, batch_targets

    def next_batch(self):

        if self.backgroundTask == None:
            batch_inputs, batch_targets = self.__prepare_batch(self.pointer)
            self.pointer += self.batch_size
        else:
            t = time.time()
            self.backgroundTask.join()
            batch_inputs, batch_targets = self.que.get()
            t = time.time() - t
            if t > 0.01:
                print("GPU was waiting data for {} secs".format(t))

        self.backgroundTask = Thread(target=lambda q, arg1: q.put(self.__prepare_batch(arg1)), args=(self.que, self.pointer))
        self.pointer += self.batch_size
        self.backgroundTask.start()

        return batch_inputs, batch_targets

    @property
    def test_set(self):
        inputs = []
        targets = []
        # print(self.batch_size, self.pointer, self.train_inputs.shape, self.train_targets.shape)
        for file in self.test_files:
            input, mask = self.file_path_to_image(file)
            inputs.append(input)
            targets.append(mask)
        return np.array(inputs, dtype=np.uint8), np.array(targets, dtype=np.uint8)

    # не дает зашумлять маски (targetы)
    # change the activated augmenters for binary masks,
    # we only want to execute horizontal crop, flip and affine transformation
    def __activator_binmasks(self, images, augmenter, parents, default):
        if augmenter.name in ["GaussianBlur", "Dropout", "GaussianNoise", "Adder", "Multiplier", "Contraster"]:
            return False
        else:
            # default value for all other augmenters
            return default

    # Read mask in compressed format [H * W * 1] bytes
    def __ReadMask(self, path):
        mask=np.zeros((256,320),dtype=bytes)
        path_parts=os.path.split(path)
        if path_parts[1][0]=='0':
            fromfile=np.load(path)
            for i in range(len(fromfile)):
                for j in range(len(fromfile[0])):
                    mask[i,j]=int(fromfile[i,j])
        else:
            with open(path, "rb") as file0:
                for i in range(256):
                    for j in range(320):
                        a=int.from_bytes(file0.read(1),byteorder='big')
                        mask[i,j]=a
        return mask

    # Make full mask from compressed mask
    def __MakeMultiMask(self, input_mask):
        mask=np.zeros((256,320,9),dtype=bool)
        for i in range(256):
            for j in range(320):
                a=int(input_mask[i,j])
                mask[i,j,a]=True
        return mask    

    # Make full mask from file
    def __ParseMask(self, path):
        mask=np.zeros((256,320,9),dtype=bool)
        path_parts=os.path.split(path)
        if path_parts[1][0]=='0':
            fromfile=np.load(path)
            for i in range(len(fromfile)):
                for j in range(len(fromfile[0])):
                    mask[i,j,int(fromfile[i,j])]=True
        else:
            with open(path, "rb") as file0:
                for i in range(256):
                    for j in range(320):
                        a=int.from_bytes(file0.read(1),byteorder='big')
                        mask[i,j,a]=True
        return mask

    def __MasksToImages(self, multimasks):
        Nim=len(multimasks)
        H=len(multimasks[0])
        W=len(multimasks[0][0])
        Ncat=len(multimasks[0][0,0])
        for l in range(Nim):
            im=np.zeros((H,W),dtype=np.uint8)
            for i in range(len(im)):
                for j in range(len(im[0])):
                    for k in range(Ncat):
                        if multimasks[l][i,j,k]:
                            im[i,j]=k*255/8
                            break   
            scipy.misc.imsave(str(l)+'.png', im)

    @staticmethod
    def PrepareImage(test_image, network):
        # Добавить поворот вертикальных изображений
        return cv2.resize(test_image, (network.IMAGE_WIDTH, network.IMAGE_HEIGHT))

def draw_results(test_inputs, test_targets, test_segmentation, test_cost, network, batch_num, n_examples_to_plot, path = "./"):
    fig, axs = plt.subplots(4, n_examples_to_plot, figsize=(n_examples_to_plot * 3, 10))
    fig.suptitle("Cost: {}".format(test_cost), fontsize=20)
    for example_i in range(n_examples_to_plot):
        axs[0][example_i].imshow(cv2.cvtColor(test_inputs[example_i], cv2.COLOR_BGR2RGB))
        axs[1][example_i].imshow(test_targets[example_i][:,:,3].astype(np.float32), cmap='gray', vmin=0, vmax=1)
        axs[2][example_i].imshow(np.reshape(test_segmentation[example_i][:,:,3], [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray', vmin=0, vmax=1)

        test_image_thresholded = np.array([0.0 if x < 0.5 else 1.0 for x in test_segmentation[example_i][:,:,3].flatten()])
        axs[3][example_i].imshow(np.reshape(test_image_thresholded, [network.IMAGE_HEIGHT, network.IMAGE_WIDTH]), cmap='gray', vmin=0, vmax=1)

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    IMAGE_PLOT_DIR = path + '/image_plots/'
    if not os.path.exists(IMAGE_PLOT_DIR):
        os.makedirs(IMAGE_PLOT_DIR)

    plt.savefig('{}/figure{}.jpg'.format(IMAGE_PLOT_DIR, batch_num))
    plt.clf() # TODO Правильно ли так удалять?

    return buf

def PreTrain(layers, 
          path,
          augmentation_seq = iaa.Sequential([]), 
          inputShape = [256, 320, 3],
          DataFolder = 'D:/CouchesTest/',
          LoadChckPntPath = None, 
          BatchSize = 2, 
          n_epochs = 300, 
          DEBUG = False, 
          CUDADevice = '0',
          WritingPeriod = 10000):
    if DEBUG:
        print("-------------------->DEBUG MODE!<-------------------------")
        os.environ["CUDA_VISIBLE_DEVICES"] = '-1'
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = CUDADevice

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

    AllChckPntPath = os.path.join(path, "Checkpoints/save/", timestamp, 'all')
    BestChckPntPath = os.path.join(path, "Checkpoints/save/", timestamp, 'best') #Почему-то плохо записывает в эту папку

    # create directory for saving models
    os.makedirs(AllChckPntPath)
    os.makedirs(BestChckPntPath)

    BATCH_SIZE = BatchSize if not DEBUG else 1

    network = Network(layers, inputShape, True)
    dataset = Dataset(BATCH_SIZE, network, DataFolder, augmentation_seq)

    with tf.Session() as sess:
        saver = tf.train.Saver(tf.global_variables(), max_to_keep = 3)
        last_saver = tf.train.Saver(tf.global_variables(), max_to_keep = 1)
        if LoadChckPntPath != None:
            ckpt = tf.train.get_checkpoint_state(LoadChckPntPath)
            if ckpt and ckpt.model_checkpoint_path:
                print('Restoring model: {}'.format(ckpt.model_checkpoint_path))
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                raise IOError('No model found in {}.'.format(LoadChckPntPath))
        else:
            sess.run(tf.global_variables_initializer())

        test_costs = []
        # Fit all training data
        global_start = time.time()
        for epoch_i in range(n_epochs):
            dataset.reset_batch_pointer()

            for batch_i in range(dataset.num_batches_in_epoch()):
                batch_num = epoch_i * dataset.num_batches_in_epoch() + batch_i + 1


                start = time.time()
                batch_inputs, batch_targets = dataset.next_batch()

                cost, _ = sess.run([network.classCost, network.classTrain_op],
                                   feed_dict={network.inputs: batch_inputs, network.targets: batch_targets})
                end = time.time()
                print('{}/{}, epoch: {}, cost: {}, batch time: {}'.format(batch_num,
                                                                          n_epochs * dataset.num_batches_in_epoch(),
                                                                          epoch_i, cost, end - start))

                if batch_num % WritingPeriod == 0 or batch_num == n_epochs * dataset.num_batches_in_epoch():
                    testTime = time.time()
                    print("Start testing")
                    n = 0
                    test_cost = 0
                    test_inputs, test_targets = dataset.test_set
                    for i in range(len(test_inputs)):
                        test_input = test_inputs[i]
                        test_target = test_targets[i]
                        test_input = np.reshape(test_input, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS))
                        test_target = np.reshape(test_target, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.CLASS_NUMBER))

                        n = n + 1
                        cost = sess.run([network.classCost],
                                                          feed_dict={network.inputs: test_input,
                                                                     network.targets: test_target})
                        test_cost += cost[0]
                    print("Testing finished in {} secs".format(time.time()-testTime))
                    test_cost /= n

                    print('Step {}, test cost: {}'.format(batch_num, test_cost))
                    test_costs.append((test_cost, batch_num))
                    print("Costs in time: ", [test_costs[x][0] for x in range(len(test_costs))])
                    min_cost = min(test_costs)
                    print("Best cost: {} in batch {}".format(min_cost[0], min_cost[1]))
                    print("Total time: {}".format(time.time() - global_start))

                    last_saver.save(sess, os.path.join(AllChckPntPath, 'model.ckpt'), global_step=batch_num)
                    if test_cost <= min_cost[0]:
                        saver.save(sess, os.path.join(BestChckPntPath, 'model.ckpt'), global_step=batch_num)

def train(layers, 
          path,
          augmentation_seq = iaa.Sequential([]), 
          inputShape = [256, 320, 3],
          DataFolder = 'D:/CouchesTest/',
          LoadChckPntPath = None, 
          BatchSize = 2, 
          n_epochs = 300, 
          DEBUG = False, 
          CUDADevice = '0',
          WritingPeriod = 10000,
          EncoderChckpntPath = None):
    if DEBUG:
        print("-------------------->DEBUG MODE!<-------------------------")
        os.environ["CUDA_VISIBLE_DEVICES"] = '-1'
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = CUDADevice

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

    AllChckPntPath = os.path.join(path, "Checkpoints/save/", timestamp, 'all')
    BestChckPntPath = os.path.join(path, "Checkpoints/save/", timestamp, 'best') #Почему-то плохо записывает в эту папку

    # create directory for saving models
    os.makedirs(AllChckPntPath)
    os.makedirs(BestChckPntPath)

    BATCH_SIZE = BatchSize if not DEBUG else 1

    network = Network(layers, inputShape, True)
    dataset = Dataset(BATCH_SIZE, network, DataFolder, augmentation_seq)

    with tf.Session() as sess:
        if LoadChckPntPath != None:
            network.Reverse(layers, True)
            saver = tf.train.Saver(tf.global_variables(), max_to_keep = 3)
            last_saver = tf.train.Saver(tf.global_variables(), max_to_keep = 1)
            ckpt = tf.train.get_checkpoint_state(LoadChckPntPath)
            if ckpt and ckpt.model_checkpoint_path:
                print('Restoring model: {}'.format(ckpt.model_checkpoint_path))
                saver.restore(sess, ckpt.model_checkpoint_path)
            else:
                raise IOError('No model found in {}.'.format(LoadChckPntPath))
        elif EncoderChckpntPath != None:
            saver = tf.train.Saver(tf.global_variables(), max_to_keep = 3)
            ckpt = tf.train.get_checkpoint_state(EncoderChckpntPath)
            if ckpt and ckpt.model_checkpoint_path:
                print('Restoring model: {}'.format(ckpt.model_checkpoint_path))
                saver.restore(sess, ckpt.model_checkpoint_path)

                network.Reverse(layers, True)
            else:
                raise IOError('No model found in {}.'.format(LoadChckPntPath))
            

            # Initialization of uninitialized variables. Not good code, but global initializer rewrite already trained vars
            for var in tf.global_variables():
                try:
                    sess.run(var)
                except tf.errors.FailedPreconditionError:
                    sess.run(var.initializer)
                    
            saver = tf.train.Saver(tf.global_variables(), max_to_keep = 3)
            last_saver = tf.train.Saver(tf.global_variables(), max_to_keep = 1)
        else:
            network.Reverse(layers, True)
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver(tf.global_variables(), max_to_keep = 3)
            last_saver = tf.train.Saver(tf.global_variables(), max_to_keep = 1)

        test_costs = []
        # Fit all training data
        global_start = time.time()
        for epoch_i in range(n_epochs):
            dataset.reset_batch_pointer()

            for batch_i in range(dataset.num_batches_in_epoch()):
                batch_num = epoch_i * dataset.num_batches_in_epoch() + batch_i + 1

                start = time.time()
                batch_inputs, batch_targets = dataset.next_batch()

                cost, _ = sess.run([network.cost, network.train_op],
                                   feed_dict={network.inputs: batch_inputs, network.targets: batch_targets})
                end = time.time()
                print('{}/{}, epoch: {}, cost: {}, batch time: {}'.format(batch_num,
                                                                          n_epochs * dataset.num_batches_in_epoch(),
                                                                          epoch_i, cost, end - start))

                if batch_num % WritingPeriod == 0 or batch_num == n_epochs * dataset.num_batches_in_epoch():
                    testTime = time.time()
                    print("Start testing")
                    n = 0
                    test_cost = 0
                    test_inputs, test_targets = dataset.test_set
                    for i in range(len(test_inputs)):
                        test_input = test_inputs[i]
                        test_target = test_targets[i]
                        test_input = np.reshape(test_input, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS))
                        test_target = np.reshape(test_target, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.CLASS_NUMBER))
 
                        n = n + 1
                        cost = sess.run([network.cost],
                                                          feed_dict={network.inputs: test_input,
                                                                     network.targets: test_target})
                        test_cost += cost[0]
                    print("Testing finished in {} secs".format(time.time()-testTime))
                    test_cost /= n

                    print('Step {}, test cost: {}'.format(batch_num, test_cost))
                    test_costs.append((test_cost, batch_num))
                    print("Costs in time: ", [test_costs[x][0] for x in range(len(test_costs))])
                    min_cost = min(test_costs)
                    print("Best cost: {} in batch {}".format(min_cost[0], min_cost[1]))
                    print("Total time: {}".format(time.time() - global_start))

                    # Plot example reconstructions
                    n_examples = 4 if not DEBUG else 4
                    test_inputs, test_targets = dataset.test_set
                    test_inputs, test_targets = test_inputs[:n_examples], test_targets[:n_examples]
                    
                    test_inputs = np.reshape(test_inputs, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.IMAGE_CHANNELS))
                    test_targets = np.reshape(test_targets, (-1, network.IMAGE_HEIGHT, network.IMAGE_WIDTH, network.CLASS_NUMBER))

                    test_segmentation = sess.run(network.segmentation_result, feed_dict={
                        network.inputs: test_inputs})

                    # Prepare the plot
                    test_plot_buf = draw_results(test_inputs, test_targets, test_segmentation, test_cost, network,
                                                 batch_num, n_examples, path = path)

                    saver.save(sess, os.path.join(AllChckPntPath, 'model.ckpt'))
                    if test_cost <= min_cost[0]:
                        saver.save(sess, os.path.join(BestChckPntPath, 'model.ckpt'), global_step=batch_num)