import time
t = time.time()

import os

import EvalClasses

print('import time: {} sec'.format(time.time()-t))
t = time.time()

def list_files(in_path):
    files = []
    for (dirpath, dirnames, filenames) in os.walk(in_path):
        files.extend(filenames)
        break
    return files

def SegmentImages(layers,
    inputShape = [256, 320, 3],
    DataFolder = 'D:/CouchesTest/',
    LoadEncoderPath = None, 
    LoadClassPath = None,
    LoadDecoderPath = None,
    SharedW = True,
    SavingPath = "./",
    ClassNumber = 9):

    OutputPath = os.path.join(SavingPath,"TestOutputs/")
    if not os.path.exists(OutputPath):
        os.makedirs(OutputPath)
        
    t = time.time()
    segmentator = EvalClasses.Segmentator(layers, inputShape, LoadEncoderPath, LoadClassPath, LoadDecoderPath, SharedW, ClassNumber)
    print('network creation time: {} sec'.format(time.time()-t))

    ImgProcessor = EvalClasses.ImageProcessor(inputShape)
    files = list_files(DataFolder)
    for file in files:
        t = time.time()
        input_file = os.path.join(DataFolder,file)
        output_file_segm = os.path.join(OutputPath,"segm_" + file)

        image = ImgProcessor.LoadBGR(input_file)

        result = segmentator.SegmentImg(image)

        segmentation = result[0][0,:,:,:]
        if LoadClassPath:
            print("Class #{}".format(result[1].argmax()))

        image = ImgProcessor.BGRtoRGB(image)
        ImgProcessor.SaveCombo(output_file_segm, image, segmentation)

        print('segmentation time: {} sec'.format(time.time()-t))
    return 1

def PrepareShadows(layers,
    inputShape = [256, 320, 3],
    DataFolder = 'D:/CouchesTest/',
    LoadEncoderPath = None, 
    LoadClassPath = None,
    LoadDecoderPath = None,
    SharedW = True,
    SavingPath = "./",
    ClassNumber = 9):

    OutputPath = os.path.join(SavingPath,"TestOutputs/")
    if not os.path.exists(OutputPath):
        os.makedirs(OutputPath)
        
    t = time.time()
    segmentator = EvalClasses.Segmentator(layers, inputShape, LoadEncoderPath, LoadClassPath, LoadDecoderPath, SharedW, ClassNumber)
    print('network creation time: {} sec'.format(time.time()-t))

    ImgProcessor = EvalClasses.ImageProcessor(inputShape)
    files = list_files(DataFolder)

    import cv2
    for file in files:
        try:
            print("Image {} processing.".format(file))

            input_file = os.path.join(DataFolder,file)
            output_file_segm = os.path.join(OutputPath,"segm_" + file)
        
            t = time.time()
            StyleImgProc = EvalClasses.ImageProcessor(PostBlurRate = 1)
            StyleImgProc.PostUseColorTransfer = False
            StyleImgProc.PostUseMeanInsteadMax = True
            StyleImgProc.PostUseWBControl = False
            StyleImgProc.PostUseSmartContrastChooser = False
            StyleImgProc.PostContrastRate = 1
            imgBGRbig = StyleImgProc.LoadBGR(input_file, True, True)
            imgRGBbig = StyleImgProc.BGRtoRGB(imgBGRbig)
            imgBGR = ImgProcessor.Resize(imgBGRbig)
            imgProcessingTime = time.time() - t

            t = time.time()
            result = segmentator.SegmentImg(imgBGR)
        
            t = time.time()
            Segm = StyleImgProc.Resize(result[0][0,:,:,:])
            Segm = StyleImgProc.ThresholdMask(Segm)
            OnlySofa = StyleImgProc.getOnlySofa(imgBGRbig, Segm)
            OnlyShadow = StyleImgProc.getShadow(imgBGRbig, Segm)
            imgProcessingTime += time.time() - t
        
            cv2.imwrite("D:/Temp/GAN/Test/testA/{}".format(file), OnlySofa)
            #cv2.imwrite("D:/Temp/Shadows/trainA/{}".format(file), OnlyShadow)
        except:
            print("ERROR!ERROR!ERROR!ERROR!ERROR!ERROR!ERROR!ERROR!ERROR!ERROR!ERROR!")
            continue

    return 1

def EvalQuality(layers,
    inputShape = [256, 320, 3],
    DataFolder = 'D:/CouchesTest/',
    LoadEncoderPath = None, 
    LoadClassPath = None,
    LoadDecoderPath = None,
    SharedW = True,
    SavingPath = None,
    ClassNumber = 9):

    OutputPath = os.path.join(SavingPath,"TestOutputs/")
    if not os.path.exists(OutputPath):
        os.makedirs(OutputPath)
        
    segmentator = EvalClasses.Segmentator(layers, inputShape, LoadEncoderPath, LoadClassPath, LoadDecoderPath, SharedW, ClassNumber)

    ImgProcessor = EvalClasses.ImageProcessor(inputShape)
    
    inputsPath = os.path.join(DataFolder, "inputs")
    masksPath = os.path.join(DataFolder, "masks")
    files = list_files(inputsPath)

    goodImgs = 0.0
    midQulity = 0.0
    with open("DifficultImages.txt", "w") as f:
        pass

    for file in files:
        input_file = os.path.join(inputsPath,file)
        output_file_segm = os.path.join(OutputPath,"segm_" + file)
        target_file = os.path.join(masksPath, file)
        target_file = target_file[:-3]
        target_file = target_file + 'png'
        
        image = ImgProcessor.LoadBGR(input_file)
        
        result = segmentator.SegmentImg(image)

        thresh = ImgProcessor.ThresholdMask(result[0][0,:,:,0])
        target = ImgProcessor.LoadMask(target_file)
        Quality = ImgProcessor.EvalQuality(thresh, target)
        print("Quality = {} on picture {}".format(Quality, file))

        midQulity += Quality

        if Quality > 0.8:
            goodImgs+=1.0
        else:
            with open("DifficultImages.txt", "a") as f:
                f.write(file + '\n')

    goodImgs /= len(files)
    midQulity /= len(files)
    print("Good images rate: {}, mid quality: {}".format(goodImgs, midQulity))
    return 1

def SegmentAndStyleImages(layers,
    inputShape = [256, 320, 3],
    DataFolder = 'D:/CouchesTest/',
    LoadEncoderPath = None, 
    LoadClassPath = None,
    LoadDecoderPath = None,
    SharedW = True,
    SavingPath = "./",
    ClassNumber = 9,
    LoadStylePath = None):

    OutputPath = os.path.join(SavingPath,"TestOutputs/")
    if not os.path.exists(OutputPath):
        os.makedirs(OutputPath)
        
    t = time.time()
    segmentator = EvalClasses.Segmentator(layers, inputShape, LoadEncoderPath, LoadClassPath, LoadDecoderPath, SharedW, ClassNumber)
    print('network creation time: {} sec'.format(time.time()-t))

    ImgProcessor = EvalClasses.ImageProcessor(inputShape)
    files = list_files(DataFolder)
    for file in files:
        print("Image {} processing.".format(file))

        input_file = os.path.join(DataFolder,file)
        output_file_segm = os.path.join(OutputPath,"segm_" + file)
        
        t = time.time()
        StyleImgProc = EvalClasses.ImageProcessor(PostBlurRate = 1)
        StyleImgProc.PostUseColorTransfer = False
        StyleImgProc.PostUseMeanInsteadMax = True
        StyleImgProc.PostUseWBControl = False
        StyleImgProc.PostUseSmartContrastChooser = False
        StyleImgProc.PostContrastRate = 1
        imgBGRbig = StyleImgProc.LoadBGR(input_file, True, True)
        imgRGBbig = StyleImgProc.BGRtoRGB(imgBGRbig)
        imgBGR = ImgProcessor.Resize(imgBGRbig)
        imgProcessingTime = time.time() - t

        t = time.time()
        result = segmentator.SegmentImg(imgBGR)
        print('segmentation time: {} sec'.format(time.time()-t))
        
        t = time.time()
        Segm = StyleImgProc.Resize(result[0][0,:,:,:])
        Segm = StyleImgProc.ThresholdMask(Segm)
        imgRGBbigTransformed = StyleImgProc.PreProcessing(imgRGBbig, Segm)
        imgProcessingTime += time.time() - t
        
        t = time.time()
        stylizer = EvalClasses.FastStylizer(LoadStylePath, imgRGBbig.shape)
        Style = stylizer.StyleImg(imgRGBbigTransformed)
        
        #Style = StyleImgProc.LoadBGR("C:/Users/dimit/Downloads/aquamarine_4.JPG")
        #Style = StyleImgProc.BGRtoRGB(Style)
        print('stylization time: {} sec'.format(time.time()-t))
        
        t = time.time()
        Style = StyleImgProc.Resize(Style)
        Style = StyleImgProc.PostProcessing(imgRGBbig, Style, Segm)
        Combo = StyleImgProc.Combine(imgRGBbig, Style, Segm)
        print('image processing time: {} sec'.format(time.time()-t+imgProcessingTime))

        StyleImgProc.SaveCombo(output_file_segm, imgRGBbig, Combo)#, StyleImgProc.get_color_diviation(imgRGBbig, Segm))
        if LoadClassPath:
            print("Class #{}".format(result[1].argmax()))

    return 1

def CompareSegmentAndStyleImages(layers,
    inputShape = [256, 320, 3],
    DataFolder = 'D:/CouchesTest/',
    LoadEncoderPath = None, 
    LoadClassPath = None,
    LoadDecoderPath = None,
    SharedW = True,
    SavingPath = "./",
    ClassNumber = 9,
    LoadStylePath = None):

    OutputPath = os.path.join(SavingPath,"TestOutputs/")
    if not os.path.exists(OutputPath):
        os.makedirs(OutputPath)
        
    t = time.time()
    segmentator = EvalClasses.Segmentator(layers, inputShape, LoadEncoderPath, LoadClassPath, LoadDecoderPath, SharedW, ClassNumber)
    print('network creation time: {} sec'.format(time.time()-t))

    ImgProcessor = EvalClasses.ImageProcessor(inputShape)
    files = list_files(DataFolder)
    for file in files:
        print("Image {} processing.".format(file))

        input_file = os.path.join(DataFolder,file)
        output_file_segm = os.path.join(OutputPath,"segm_" + file)
        
        t = time.time()
        StyleImgProc1 = EvalClasses.ImageProcessor(PostBlurRate = 30)
        StyleImgProc1.PostUseColorTransfer = False
        StyleImgProc1.PostUseMeanInsteadMax = True
        StyleImgProc1.PostUseWBControl = False
        StyleImgProc1.PostUseSmartContrastChooser = False
        StyleImgProc1.PostContrastRate = 1
        StyleImgProc2 = EvalClasses.ImageProcessor(PostBlurRate = 30)
        StyleImgProc2.PostUseColorTransfer = False
        StyleImgProc2.PostUseMeanInsteadMax = True
        StyleImgProc2.PostUseWBControl = False
        StyleImgProc2.PostUseSmartContrastChooser = False
        StyleImgProc2.PostContrastRate = 1
        imgBGRbig = StyleImgProc1.LoadBGR(input_file, True, True)
        imgBGRbig = StyleImgProc2.LoadBGR(input_file, True, True)
        imgRGBbig = StyleImgProc1.BGRtoRGB(imgBGRbig)
        imgBGR = ImgProcessor.Resize(imgBGRbig)
        imgProcessingTime = time.time() - t

        t = time.time()
        result = segmentator.SegmentImg(imgBGR)
        print('segmentation time: {} sec'.format(time.time()-t))
        
        t = time.time()
        Segm = StyleImgProc1.Resize(result[0][0,:,:,:])
        Segm = StyleImgProc1.ThresholdMask(Segm)
        imgRGBbigTransformed1 = StyleImgProc1.PreProcessing(imgRGBbig, Segm)
        imgRGBbigTransformed2 = StyleImgProc2.PreProcessing(imgRGBbig, Segm)
        imgProcessingTime += time.time() - t
        
        t = time.time()
        stylizer = EvalClasses.FastStylizer(LoadStylePath, imgRGBbig.shape)
        Style1 = stylizer.StyleImg(imgRGBbigTransformed1)
        Style2 = stylizer.StyleImg(imgRGBbigTransformed2)
        Style2 = StyleImgProc1.LoadBGR("C:/Users/dimit/Downloads/IMG_8948_Extracted.JPG")
        Style2 = StyleImgProc1.BGRtoRGB(Style2)
        print('stylization time: {} sec'.format(time.time()-t))
        
        t = time.time()
        Style1 = StyleImgProc1.Resize(Style1)
        Style1 = StyleImgProc1.PostProcessing(imgRGBbig, Style1, Segm)
        Combo1 = StyleImgProc1.Combine(imgRGBbig, Style1, Segm)
        Style2 = StyleImgProc2.Resize(Style2)
        Style2 = StyleImgProc2.PostProcessing(imgRGBbig, Style2, Segm)
        Combo2 = StyleImgProc2.Combine(imgRGBbig, Style2, Segm)
        print('image processing time: {} sec'.format(time.time()-t+imgProcessingTime))

        StyleImgProc1.SaveCombo(output_file_segm, Combo1, Combo2)#, StyleImgProc.get_color_diviation(imgRGBbig, Segm))
        if LoadClassPath:
            print("Class #{}".format(result[1].argmax()))

    return 1