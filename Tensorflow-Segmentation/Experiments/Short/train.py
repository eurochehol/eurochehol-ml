# Activations Test
import os
import sys
sys.path.append("../../")
import autoencoder

from imgaug import augmenters as iaa
from imgaug import imgaug
from conv2d import Conv2d
from max_pool_2d import MaxPool2d

def CreateLayers():
    layers = []
    #Short
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_1_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_1_2'))
    layers.append(MaxPool2d(kernel_size=2, name='max_1'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=128, name='conv_2_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=128, name='conv_2_2'))
    layers.append(MaxPool2d(kernel_size=2, name='max_2'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=256, name='conv_3_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=256, name='conv_3_2'))
    layers.append(MaxPool2d(kernel_size=2, name='max_3'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_4_1'))
    #Short

    return layers

InputShape = [256, 320, 3]

augmentation_seq = iaa.Sequential([
        iaa.Add(value = (-45, 45), per_channel = True, name="Adder"),
        iaa.Multiply(mul = (0.75, 1.25), per_channel = True, name="Multiplier"),
        iaa.ContrastNormalization(alpha = (0.5, 1.5), name="Contraster"),

        # Эти шумы отключены, так как могут исказить запоминаемые текстуры
        #iaa.GaussianBlur((0, 3.0), name="GaussianBlur"),
        #iaa.Dropout(0.02, name="Dropout"),
        #iaa.AdditiveGaussianNoise(scale=0.01, name="GaussianNoise"),

        iaa.Crop(px=(0, 16), name="Cropper"),  # crop images from each side by 0 to 16px (randomly chosen)
        iaa.Fliplr(0.5, name="Flipper"),
        iaa.Affine(translate_px={"x": (InputShape[0] // 3, InputShape[1] // 3)}, name="Affine")
    ])
if __name__ == '__main__':
    autoencoder.train(CreateLayers(), os.path.dirname(os.path.abspath(__file__)), augmentation_seq, InputShape, DEBUG = False)
