import os
import sys
sys.path.append("../../")
import autoencoderRSepFinal as autoencoder

from imgaug import augmenters as iaa
from imgaug import imgaug
from conv2d import Conv2d
from max_pool_2d import MaxPool2d

def CreateLayers():
    layers = []
    
    #VGG19
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_1_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_1_2'))
    layers.append(MaxPool2d(kernel_size=2, name='max_1'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=128, name='conv_2_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=128, name='conv_2_2'))
    layers.append(MaxPool2d(kernel_size=2, name='max_2'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=256, name='conv_3_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=256, name='conv_3_2'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=256, name='conv_3_3'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=256, name='conv_3_4'))
    layers.append(MaxPool2d(kernel_size=2, name='max_3'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_4_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_4_2'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_4_3'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_4_4'))
    layers.append(MaxPool2d(kernel_size=2, name='max_4'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_5_1'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_5_2'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_5_3'))
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=512, name='conv_5_4'))
    layers.append(MaxPool2d(kernel_size=2, name='max_5'))
    #VGG19

    return layers

InputShape = [256, 320, 3]

augmentation_seq = iaa.Sequential([
        iaa.Add(value = (-45, 45), per_channel = True, name="Adder"),
        iaa.Multiply(mul = (0.75, 1.25), per_channel = True, name="Multiplier"),
        iaa.ContrastNormalization(alpha = (0.5, 1.5), name="Contraster"),

        # Эти шумы отключены, так как могут исказить запоминаемые текстуры
        #iaa.GaussianBlur((0, 3.0), name="GaussianBlur"),
        #iaa.Dropout(0.02, name="Dropout"),
        #iaa.AdditiveGaussianNoise(scale=0.01, name="GaussianNoise"),

        iaa.Crop(px=(0, 40), name="Cropper"),  # crop images from each side by 0 to 16px (randomly chosen)
        iaa.Fliplr(0.5, name="Flipper"),
        #iaa.Affine(translate_px={"x": (InputShape[0] // 3, InputShape[1] // 3)}, name="Affine")
    ])

WinDataPath = 'D:/CouchesTest/'
LinDataPath = '/root/Data/CouchesFiltered/'

WinEncoderPath = 'D:/Temp/Final/F05.02/last/encoder/model.ckpt-35600'
WinClassPath = None
WinDecoderPath = 'D:/Temp/Final/F05.02/last/decoder/model.ckpt-35600'

LinEncoderPath = '/root/Data/Checkpoints/Sum/last/encoder/'
LinClassPath = ''
LinDecoderPath = ''

WinSavingPath = 'D:/Temp/save/100'
LinSavingPath = '/root/Data/Checkpoints/Shared/'

def TestWin():
    print("Start encoder training testing.")
    autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = WinDataPath,
          LoadEncoderPath = WinEncoderPath, 
          LoadClassPath = WinClassPath,
          LoadDecoderPath = WinDecoderPath,
          BATCH_SIZE = 1, 
          n_epochs = 2, 
          CUDADevice = '0',
          WritingPeriod = 14,
          SavingPath = WinSavingPath,
          TrainEncoder = True,
          TrainDecoder = True,
          TrainClass = True,
          ClassNumber = 9,
          PlotExamples = 4)

def TestLin1():
    print("Start encoder training testing.")
    autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = LinDataPath,
          LoadEncoderPath = None, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 8, 
          n_epochs = 3, 
          CUDADevice = '0',
          WritingPeriod = 50000,
          SavingPath = None,
          TrainEncoder = True,
          TrainDecoder = True,
          TrainClass = True,
          ClassNumber = 9,
          PlotExamples = 20)

def TestLin2():
    print("Start encoder training testing.")
    autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = LinDataPath,
          LoadEncoderPath = None, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 8, 
          n_epochs = 3, 
          CUDADevice = '0',
          WritingPeriod = 50000,
          SavingPath = None,
          TrainEncoder = False,
          TrainDecoder = True,
          TrainClass = False,
          ClassNumber = 9,
          PlotExamples = 20)

def TestLin3():
    print("Start encoder training testing.")
    autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = LinDataPath,
          LoadEncoderPath = LinEncoderPath, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 4, 
          n_epochs = 100, 
          CUDADevice = '0',
          WritingPeriod = 50000,
          SavingPath = LinSavingPath,
          TrainEncoder = True,
          TrainDecoder = True,
          TrainClass = False,
          ClassNumber = 9,
          PlotExamples = 20)

if __name__ == '__main__':
    TestWin()