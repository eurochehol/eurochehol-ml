import sys
sys.path.append("../../")
import evalRSepFinal as eval
import train
import os

PlotPath = "D:/Temp/Plots/new/"
#TestImagesPath = "D:/Test/inputs/"
#TestImagesPath = "D:/Temp/moskva/"
#TestImagesPath = "D:/TestInputs/"
TestImagesPath = "D:/EasyInputs/"
EvalImagesPath = "D:/Test/"

LoadStylePath = "D:/Temp/checkpoint_22.17/fns.ckpt"

def Segment():
    eval.SegmentImages(layers = train.CreateLayers(),
              inputShape = train.InputShape,
              DataFolder = TestImagesPath,
              LoadEncoderPath = train.WinEncoderPath, 
              LoadClassPath = train.WinClassPath,
              LoadDecoderPath = train.WinDecoderPath,
              SharedW = True,
              SavingPath = PlotPath,
              ClassNumber = 9)

def Eval():
    eval.EvalQuality(layers = train.CreateLayers(),
              inputShape = train.InputShape,
              DataFolder = EvalImagesPath,
              LoadEncoderPath = train.WinEncoderPath, 
              LoadClassPath = train.WinClassPath,
              LoadDecoderPath = train.WinDecoderPath,
              SharedW = True,
              SavingPath = PlotPath,
              ClassNumber = 9)

def SegmAndStyle():
    eval.SegmentAndStyleImages(layers = train.CreateLayers(),
              inputShape = train.InputShape,
              DataFolder = TestImagesPath,
              LoadEncoderPath = train.WinEncoderPath, 
              LoadClassPath = train.WinClassPath,
              LoadDecoderPath = train.WinDecoderPath,
              SharedW = True,
              SavingPath = PlotPath,
              ClassNumber = 9,
              LoadStylePath = LoadStylePath)

def Compare():
    eval.CompareSegmentAndStyleImages(layers = train.CreateLayers(),
              inputShape = train.InputShape,
              DataFolder = TestImagesPath,
              LoadEncoderPath = train.WinEncoderPath, 
              LoadClassPath = train.WinClassPath,
              LoadDecoderPath = train.WinDecoderPath,
              SharedW = True,
              SavingPath = PlotPath,
              ClassNumber = 9,
              LoadStylePath = LoadStylePath)

if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = '0'
    SegmAndStyle()