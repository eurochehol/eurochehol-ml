"""Activations for TensorFlow.
Parag K. Mital, Jan 2016."""
import tensorflow as tf
import math


def lrelu(x, leak=0.2, name="lrelu"):
    """Leaky rectifier.

    Parameters
    ----------
    x : Tensor
        The tensor to apply the nonlinearity to.
    leak : float, optional
        Leakage parameter.
    name : str, optional
        Variable scope to use.

    Returns
    -------
    x : Tensor
        Output of the nonlinearity.
    """
    with tf.variable_scope(name):
        f1 = 0.5 * (1 + leak)
        f2 = 0.5 * (1 - leak)
        return f1 * x + f2 * abs(x)

def sigmoid(x, name="sigmoid"):
    with tf.variable_scope(name):
        return 1.0 / (1.0 + math.exp(-x))

def linear(x, name="linear"):
    with tf.variable_scope(name):
        return x