import unittest
import os
import sys
sys.path.append("../../")
import autoencoderRSepFinal as autoencoder
import evalRSepFinal as eval

from imgaug import augmenters as iaa
from imgaug import imgaug
from conv2d import Conv2d
from max_pool_2d import MaxPool2d

def CreateLayers():
    layers = []
    
    #Test
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=64, name='conv_1_1'))
    layers.append(MaxPool2d(kernel_size=2, name='max_1'))
            
    layers.append(Conv2d(kernel_size=3, strides=[1, 1, 1, 1], output_channels=128, name='conv_2_1'))
    layers.append(MaxPool2d(kernel_size=2, name='max_2'))
    #Test

    return layers

InputShape = [256, 320, 3]

augmentation_seq = iaa.Sequential([
        iaa.Add(value = (-45, 45), per_channel = True, name="Adder"),
        iaa.Multiply(mul = (0.75, 1.25), per_channel = True, name="Multiplier"),
        iaa.ContrastNormalization(alpha = (0.5, 1.5), name="Contraster"),

        # Эти шумы отключены, так как могут исказить запоминаемые текстуры
        #iaa.GaussianBlur((0, 3.0), name="GaussianBlur"),
        #iaa.Dropout(0.02, name="Dropout"),
        #iaa.AdditiveGaussianNoise(scale=0.01, name="GaussianNoise"),

        iaa.Crop(px=(0, 40), name="Cropper"),  # crop images from each side by 0 to 16px (randomly chosen)
        iaa.Fliplr(0.5, name="Flipper"),
        #iaa.Affine(translate_px={"x": (InputShape[0] // 3, InputShape[1] // 3)}, name="Affine")
    ])

WinDataPath = 'D:/CouchesTest/'

WinEncoderPath = 'D:/Temp/Test/save/last/encoder/'
WinClassPath = 'D:/Temp/Test/save/last/class/'
WinDecoderPath = 'D:/Temp/Test/save/last/decoder/'

SavingPath = 'D:/Temp/Test/save/'
SavingSepWeigthsPath = 'D:/Temp/TestSepW/save/'
SavingDifClassPath = 'D:/Temp/TestDifC/save/'

PlotPath = "D:/Temp/Test/Plots/"
TestImagesPath = "D:/Temp/TestInputs/"

class Test_test1(unittest.TestCase):
    def test_encoder_empty(self):
        result = autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = WinDataPath,
          LoadEncoderPath = None, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 2, 
          n_epochs = 2, 
          CUDADevice = '0',
          WritingPeriod = 14,
          SavingPath = SavingPath,
          TrainEncoder = True,
          TrainDecoder = False,
          TrainClass = True,
          ClassNumber = 9,
          PlotExamples = 4)
        self.assertEqual(result, 1)

    def test_combo_empty(self):
        result = autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = WinDataPath,
          LoadEncoderPath = None, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 2, 
          n_epochs = 2, 
          CUDADevice = '0',
          WritingPeriod = 14,
          SavingPath = SavingPath,
          TrainEncoder = True,
          TrainDecoder = True,
          TrainClass = True,
          ClassNumber = 9,
          PlotExamples = 4)
        self.assertEqual(result, 1)

    def test_combo_from_chckpnt(self):
        result = autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = WinDataPath,
          LoadEncoderPath = WinEncoderPath, 
          LoadClassPath = WinClassPath,
          LoadDecoderPath = WinDecoderPath,
          BATCH_SIZE = 2, 
          n_epochs = 2, 
          CUDADevice = '0',
          WritingPeriod = 14,
          SavingPath = SavingPath,
          TrainEncoder = True,
          TrainDecoder = True,
          TrainClass = True,
          ClassNumber = 9,
          PlotExamples = 4)
        self.assertEqual(result, 1)

    def test_decoder_from_encoder(self):
        result = autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = WinDataPath,
          LoadEncoderPath = WinEncoderPath, 
          LoadClassPath = None,
          LoadDecoderPath = None,
          BATCH_SIZE = 2, 
          n_epochs = 2, 
          CUDADevice = '0',
          WritingPeriod = 14,
          SavingPath = SavingSepWeigthsPath,
          TrainEncoder = False,
          TrainDecoder = True,
          TrainClass = False,
          ClassNumber = 9,
          PlotExamples = 4)
        self.assertEqual(result, 1)

    def test_change_class(self):
        result = autoencoder.TrainEncoder(layers = CreateLayers(), 
          path = os.path.dirname(os.path.abspath(__file__)),
          augmentation_seq = augmentation_seq, 
          inputShape = InputShape,
          DataFolder = WinDataPath,
          LoadEncoderPath = WinEncoderPath, 
          LoadClassPath = None,
          LoadDecoderPath = WinDecoderPath,
          BATCH_SIZE = 2, 
          n_epochs = 2, 
          CUDADevice = '0',
          WritingPeriod = 14,
          SavingPath = SavingSepWeigthsPath,
          TrainEncoder = True,
          TrainDecoder = False,
          TrainClass = True,
          ClassNumber = 5,
          PlotExamples = 4)
        self.assertEqual(result, 1)

    def test_eval_wrong_arg(self):
        result = eval.eval(layers = CreateLayers(),
            inputShape = InputShape,
            DataFolder = TestImagesPath,
            LoadEncoderPath = WinEncoderPath, 
            LoadClassPath = None,
            LoadDecoderPath = None,
            SharedW = True,
            CUDADevice = '0',
            SavingPath = PlotPath,
            ClassNumber = 9)
        self.assertEqual(result, -1)

    def test_eval(self):
        result = eval.eval(layers = CreateLayers(),
            inputShape = InputShape,
            DataFolder = TestImagesPath,
            LoadEncoderPath = WinEncoderPath, 
            LoadClassPath = WinClassPath,
            LoadDecoderPath = WinDecoderPath,
            SharedW = True,
            CUDADevice = '-1',
            SavingPath = PlotPath,
            ClassNumber = 9)
        self.assertEqual(result, 1)
if __name__ == '__main__':
    unittest.main()
